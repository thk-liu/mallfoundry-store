const AntdDayjsWebpackPlugin = require("antd-dayjs-webpack-plugin")
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer")
// const CracoAntDesignPlugin = require("craco-antd")
module.exports = {
  babel: {
    plugins: [
      ["import", { libraryName: "antd", libraryDirectory: "lib", style: "css" }],
      "@babel/plugin-proposal-class-properties",
    ],
  },
  webpack: {
    plugins: [
      new AntdDayjsWebpackPlugin(),
      // new BundleAnalyzerPlugin(),
    ],
  },
  /*  plugins: [{
      plugin: CracoAntDesignPlugin,
    }],*/
}
