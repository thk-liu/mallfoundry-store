import { Carrier, CarrierService } from "@mallfoundry/shipping"
import * as _ from "lodash"
import { useEffect, useState } from "react"

export function useCarriers() {
  const [carriers, setCarriers] = useState([] as Carrier[])
  useEffect(() => {
    CarrierService.getCarriers().then(setCarriers)
  }, [])
  return [carriers]
}

export function useCarrier(code: string) {
  const [carriers] = useCarriers()
  const carrier = _.find(carriers, carrier => carrier.code === code) as Carrier
  return [carrier]
}
