import { Card, PageHeader } from "antd"
import * as React from "react"
import classes from "./shipping-rate-new.module.scss"

export default function ShippingRateNew() {
  return (
    <div className={classes.ShippingRateNew}>
      <PageHeader title="新建运费模板" ghost={false}/>
      <Card className={classes.ShippingRateNewContent}>
        {/* <ShippingRateForm/>*/}
      </Card>
    </div>
  )
}
