import { Button, Card, Col, Collapse, PageHeader, Row } from "antd"
import * as React from "react"
import { useHistory, useParams } from "react-router-dom"
import classes from "./shipping-rate-list.module.scss"

const { Panel } = Collapse

export default function ShippingRateList() {
  const history = useHistory()
  const { storeId = "" } = useParams<{ storeId: string }>()

  return (
    <div className={classes.shippingRateList}>
      <PageHeader title="运费价格" ghost={false}/>
      <Card className={classes.shippingRateListContent}>
        <Row gutter={[16, 16]}>
          <Col><Button type="primary"
                       onClick={() => history.push(`/stores/${storeId}/shipping-rates/new`)}>新建运费价格</Button></Col>
        </Row>
        <Row>
          <Col span={24}>
            <Collapse defaultActiveKey={["1"]}>
              <Panel header="This is panel header 1" key="1" showArrow={false}>
                <p>This is panel header 1</p>
              </Panel>
              <Panel header="This is panel header 2" key="2" showArrow={false}>
                <p>This is panel header 1</p>
              </Panel>
              <Panel header="This is panel header 3" key="3" showArrow={false}>
                <p>This is panel header 1</p>
              </Panel>
            </Collapse>
          </Col>
        </Row>
      </Card>
    </div>
  )
}
