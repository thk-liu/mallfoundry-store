import { BankCard, BankCardFunding, BankCardService, HolderType } from "@mallfoundry/finance"
import { Form, Input, Modal, Select } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useEffect, useState } from "react"

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
}

interface BankCardEditProps {
  visible?: boolean
  card?: BankCard
  onOk?: () => void
  onCancel?: () => void
}

export function BankCardEditModal(props: BankCardEditProps) {
  const { visible = false, onOk, onCancel } = props
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    if (!_.isUndefined(props.card)) {
      form.setFieldsValue(props.card)
    }
  }, [form, props.card])

  function handleEdit() {
    form.validateFields()
      .then(values => {
        setConfirmLoading(true)
        BankCardService
          .updateBankCard(values as BankCard)
          .then(onOk)
          .finally(() => setConfirmLoading(false))
      })
  }

  return (
    <Modal title="修改银行卡" visible={visible} confirmLoading={confirmLoading}
           onOk={handleEdit} onCancel={onCancel}>
      <Form  {...layout} name="EditCardForm" form={form}>
        <Form.Item name="id" hidden>
          <Input/>
        </Form.Item>
        <Form.Item label="持卡人类型" name="holderType"
                   rules={[{ required: true, message: "请输入银行卡类型" }]}>
          <Select placeholder="请选择持卡人类型">
            <Select.Option value={HolderType.Individual}>对私</Select.Option>
            <Select.Option value={HolderType.Company}>对公</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="持卡人" name="holderName" rules={[{ required: true, message: "请输入持卡人名称" }]}>
          <Input/>
        </Form.Item>
        {/*<Form.Item label="开户地区" name="username"
                   rules={[{ required: true, message: "Please input your username!" }]}>
          <Input/>
        </Form.Item>*/}
        <Form.Item label="银行卡类型" name="funding"
                   rules={[{ required: true, message: "请输入银行卡类型" }]}>
          <Select placeholder="请选择银行卡类型">
            <Select.Option value={BankCardFunding.Debit}>储蓄卡</Select.Option>
            <Select.Option value={BankCardFunding.Credit}>信用卡</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="银行卡号" name="number"
                   rules={[{ required: true, message: "请输入银行卡号" }]}>
          <Input/>
        </Form.Item>
        <Form.Item label="开户银行" name="bankName"
                   rules={[{ required: true, message: "请输入开户银行" }]}>
          <Input/>
        </Form.Item>
        <Form.Item label="支行名称" name="branchName"
                   rules={[{ required: true, message: "请输入支行名称" }]}>
          <Input/>
        </Form.Item>
        <Form.Item label="手机号码" name="phone"
                   rules={[{ required: true, message: "Please input your username!" }]}>
          <Input/>
        </Form.Item>
      </Form>
    </Modal>
  )
}
