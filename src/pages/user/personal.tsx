import UserOutlined from "@ant-design/icons/UserOutlined"
import { Avatar, Button, Divider, Form, Input, Radio } from "antd"
import * as React from "react"
import Page from "../../components/page"
import classes from "./personal.module.scss"

const { TextArea } = Input

export default function Personal() {

  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 12 },
  }

  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  }

  return (<div className={classes.root}>
    <Page.Header title="个人用户设置" ghost={false}/>
    <Divider/>
    <Page.Content>
      <Form   {...layout} size="large" name="personal">
        <Form.Item label="账号" name="industry">
          <span className="ant-form-text">China</span>
        </Form.Item>
        <Form.Item label="头像" name="avatar">
          <Input hidden/>
          <Avatar size={64} icon={<UserOutlined/>}/>
        </Form.Item>
        <Form.Item label="昵称" name="nickname">
          <Input placeholder="请填写用户昵称"/>
        </Form.Item>
        <Form.Item label="性别" name="gender">
          <Radio.Group>
            <Radio value="a">男</Radio>
            <Radio value="b">女</Radio>
            <Radio value="c">保密</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="个性签名" name="gender">
          <TextArea autoSize={{ minRows: 4, maxRows: 6 }}/>
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">确定修改</Button>
        </Form.Item>
      </Form>
    </Page.Content>
  </div>)
}
