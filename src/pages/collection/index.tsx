import { Collection, CollectionQuery, CollectionService } from "@mallfoundry/catalog"
import { useEffect, useState } from "react"

export function useCollections(storeId: string) {
  const [collections, setCollections] = useState<Collection[]>([])
  useEffect(() => {
    CollectionService.getCollections(new CollectionQuery().toBuilder().storeId(storeId).build())
      .then(pageList => pageList.elements)
      .then(setCollections)
  }, [storeId])

  return [collections]
}
