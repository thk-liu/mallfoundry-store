import { Collection } from "@mallfoundry/catalog"
import { Modal } from "antd"
import * as React from "react"
import CollectionForm from "./collection-form"

interface CollectionEditModalProps {
  visible?: boolean
  onCancel?: () => void
  collection?: Collection
  onFinish?: (collection: Collection) => void
}

export function CollectionEditModal(props: CollectionEditModalProps) {
  const { visible, collection, onCancel, onFinish } = props

  return (
    <Modal visible={visible}
           title="编辑商品集合"
           onCancel={onCancel}
           footer={false}>
      <CollectionForm collection={collection} onFinish={onFinish}/>
    </Modal>
  )
}
