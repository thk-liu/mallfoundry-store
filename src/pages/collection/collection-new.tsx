import { Collection } from "@mallfoundry/catalog"
import { Modal } from "antd"
import * as React from "react"
import CollectionForm from "./collection-form"

interface CollectionNewModalProps {
  visible?: boolean
  onCancel?: () => void
  onFinish?: (collection: Collection) => void
}

export function CollectionNewModal(props: CollectionNewModalProps) {
  const { visible, onCancel, onFinish } = props

  return (
    <Modal visible={visible}
           title="新建商品集合"
           onCancel={onCancel}
           footer={false}>
      <CollectionForm onFinish={onFinish}/>
    </Modal>
  )
}
