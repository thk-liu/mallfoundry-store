import { Collection } from "@mallfoundry/catalog"
import { Button, Form, Input } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useEffect } from "react"


interface CollectionFormProps {
  collection?: Collection
  onFinish?: (collection: Collection) => void
}

export default function CollectionForm(props: CollectionFormProps) {
  const { collection } = props
  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 18 },
  }
  const tailLayout = {
    wrapperCol: { offset: 5, span: 18 },
  }

  const [form] = Form.useForm()

  useEffect(() =>
      form.setFieldsValue(_.isUndefined(collection) ? {} : collection),
    [form, collection])

  function onFinish(values: any) {
    if (_.isFunction(props.onFinish)) {
      props.onFinish(_.assign(new Collection(), collection, values))
    }
  }

  return (
    <Form  {...layout} form={form}
           onFinish={onFinish}>
      <Form.Item
        label="集合名称"
        name="name"
        rules={[{ required: true, message: "请输入集合名称!" }]}>
        <Input/>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">保存</Button>
      </Form.Item>
    </Form>
  )
}
