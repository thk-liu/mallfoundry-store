import { Collection, CollectionService } from "@mallfoundry/catalog"
import { Button, Card, Col, Divider, PageHeader, Popconfirm, Row, Table } from "antd"
import { ColumnProps } from "antd/lib/table"
import * as React from "react"
import { useState } from "react"
import { Link, useParams } from "react-router-dom"
import { useCollections } from "../../hooks/collection"
import { CollectionEditModal } from "./collection-edit"

import classes from "./collection-list.module.scss"
import { CollectionNewModal } from "./collection-new"

interface RouterParams {
  storeId: string
}

export default function CollectionList() {
  const { storeId = "" } = useParams<RouterParams>()
  const [currentCollection, setCurrentCollection] = useState(new Collection())
  const [newModalVisible, setNewModalVisible] = useState(false)
  const [editModalVisible, setEditModalVisible] = useState(false)
  const [loading, setLoading] = useState(false)

  const { elements: collections } = useCollections({
    storeId,
  })

  console.log(collections)

  function handleNewCollection(collection: Collection) {
    setLoading(true)
    collection.storeId = storeId
    CollectionService.addCollection(collection)
      .finally(() => setNewModalVisible(false))
      .finally(() => setLoading(false))
  }

  function handleUpdateCollection(collection: Collection) {
    setLoading(true)
    CollectionService.updateCollection(collection)
      .finally(() => setEditModalVisible(false))
      .finally(() => setLoading(false))
  }

  function onDeleteCustomCollection(collectionId: string) {
    CollectionService.deleteCollection(collectionId)
      .finally(() => setLoading(false))
  }

  const columns: ColumnProps<Collection>[] = [
    {
      title: "分组名称",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "商品数",
      dataIndex: "productsCount",
      key: "productsCount",
      width: "120px",
      align: "center",
      render: (productsCount: number, collection) => (
        <Link to={{ pathname: `/stores/${storeId}/products`, search: `?collections=${collection.id}` }} children={productsCount}/>),
    },
    {
      title: "创建时间",
      dataIndex: "createdTime",
      key: "createdTime",
      width: "200px",
    },
    {
      title: "操作",
      key: "actions",
      width: "200px",
      render: (text: string, collection) => (
        <>
          <Button type="link" size="small" style={{ margin: 0 }}
                  onClick={() => {
                    setCurrentCollection(collection)
                    setEditModalVisible(true)
                  }}>修改</Button>
          <Divider type="vertical"/>
          <Popconfirm
            placement="bottomRight"
            onConfirm={() => onDeleteCustomCollection(collection.id as string)}
            title="确定删除这个商品?">
            <Button type="link" size="small" style={{ margin: 0 }}>删除</Button>
          </Popconfirm>
        </>
      ),
    },
  ]

  return (
    <div className={classes.CollectionList}>
      <PageHeader title="商品集合" ghost={false}/>
      <Card className={classes.CollectionListContent}>
        <Row gutter={[16, 16]}>
          <Col><Button type="primary" onClick={() => setNewModalVisible(true)}>新建商品集合</Button></Col>
          <CollectionNewModal visible={newModalVisible}
                              onCancel={() => setNewModalVisible(false)}
                              onFinish={handleNewCollection}/>
          <CollectionEditModal collection={currentCollection}
                               visible={editModalVisible}
                               onCancel={() => setEditModalVisible(false)}
                               onFinish={handleUpdateCollection}/>
        </Row>
        <Row>
          <Col span={24}>
            <Table rowKey="id" loading={loading} columns={columns} dataSource={collections}/>
          </Col>
        </Row>
      </Card>
    </div>
  )
}
