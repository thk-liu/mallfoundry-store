import { ProductType } from "@mallfoundry/catalog"
import { useEffect, useState } from "react"

interface ProductTypeOption {
  value: ProductType
  text: string
}

export function useProductTypes(storeId: string) {
  const [types, setTypes] = useState([] as ProductTypeOption[])
  useEffect(() => {
    setTypes([{
      value: ProductType.Physical,
      text: "实物商品",
    }, {
      value: ProductType.Digital,
      text: "虚拟商品",
    }])
  }, [storeId])

  return [types]
}
