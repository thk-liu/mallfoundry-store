import * as React from "react"
import classes from "./product-form-region.module.scss"

interface ProductFormRegionProps {
  title: string
  children: React.ReactNode
}

export default function ProductFormRegion(props: ProductFormRegionProps) {
  const { title, children } = props
  return (
    <div className={classes.ProductFormRegion}>
      <div className={classes.ProductFormRegionTitle}>{title}</div>
      {children}
    </div>
  )
}
