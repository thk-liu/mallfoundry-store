import { Button, Card, Col, DatePicker, Form, Input, Radio, Row, Select, Space, Table } from "antd"
import { ColumnProps } from "antd/lib/table"
import * as React from "react"
import Page from "../../../components/page"

import classes from "./products-review-list.module.scss"

const { RangePicker } = DatePicker


class OrderDisputeColumn {
  public storeId: string = ""
  public orderId: string = ""
  public refundId: string = ""
  public name: string = ""
  public itemStatus: string = ""
  public amount: string = ""
  public itemAmount: string = ""
  public appliedTime: string = ""
  public status: string = ""
  public itemsCount: number = 0
}

const columns: ColumnProps<OrderDisputeColumn>[] = [
  {
    title: "评价内容",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "星级评价",
    dataIndex: "itemStatus",
    key: "itemStatus",
    width: "160px",
    align: "center",

  },
  {
    title: "服务评价",
    dataIndex: "itemAmount",
    key: "itemAmount",
    width: "160px",
    align: "center",
  },
  {
    title: "评价时间",
    dataIndex: "amount",
    key: "amount",
    width: "160px",
    align: "center",
  },
  {
    title: "买家",
    dataIndex: "appliedTime",
    key: "appliedTime",
    width: "160px",
    align: "center",
  },
  {
    title: "操作",
    dataIndex: "status",
    key: "status",
    width: "160px",
    align: "center",

  },
]

interface ProductsReviewListProps {

}

export default function ProductsReviewList(props: ProductsReviewListProps) {
  const [form] = Form.useForm()
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
  }

  return (
    <div className={classes.ProductsReviewList}>
      <Page.Header title="售后评价" ghost={false}/>
      <Page.Content>
        <Card>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Card className={classes.FilterBar} bordered={false}>
                <Form {...layout} form={form}>
                  <Row>
                    <Col span={6}>
                      <Form.Item label="评价时间" name="appliedTimes">
                        <RangePicker style={{ width: "100%" }}/>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Radio.Group style={{ marginLeft: "8px" }}>
                        <Space>
                          <Radio.Button value={-7} className={classes.PlacedDateButton}>今</Radio.Button>
                          <Radio.Button value={-7} className={classes.PlacedDateButton}>昨</Radio.Button>
                          <Radio.Button value={-7} className={classes.PlacedDateButton}>近7天</Radio.Button>
                          <Radio.Button value={-30} className={classes.PlacedDateButton}>近30天</Radio.Button>
                        </Space>
                      </Radio.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Form.Item name="ids" label="订单编号">
                        <Input placeholder="请输入订单编号" allowClear/>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="ids" label="商品名称">
                        <Input placeholder="请输入商品名称" allowClear/>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="ids" label="评价星级">
                        <Select placeholder="请选择状态">
                          <Select.Option value="male">全部</Select.Option>
                          <Select.Option value="female">待发货</Select.Option>
                          <Select.Option value="other">已收货</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Form.Item name="after_services" label="评价方式">
                        <Select placeholder="请选择状态">
                          <Select.Option value="male">全部</Select.Option>
                          <Select.Option value="female">待发货</Select.Option>
                          <Select.Option value="other">已收货</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="after_services" label="置顶状态">
                        <Select placeholder="请选择状态">
                          <Select.Option value="male">全部</Select.Option>
                          <Select.Option value="female">待发货</Select.Option>
                          <Select.Option value="other">已收货</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Row>
                        <Col offset={6}>
                          <Space>
                            <Button type="primary">筛选</Button>
                            <Button>重置</Button>
                          </Space>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Form>
              </Card>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Radio.Group defaultValue="">
                <Radio.Button value="a">全部评价</Radio.Button>
                <Radio.Button value="b">用户自评</Radio.Button>
                <Radio.Button value="c">带图评价</Radio.Button>
                <Radio.Button value="d">加精评价</Radio.Button>
                <Radio.Button value="e">默认评价</Radio.Button>
              </Radio.Group>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Table columns={columns} pagination={false}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
