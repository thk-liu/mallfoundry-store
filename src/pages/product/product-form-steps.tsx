import classNames from "classnames"
import * as _ from "lodash"
import * as React from "react"
import classes from "./product-form-steps.module.scss"

interface ProductFormStepProps {
  children?: React.ReactNode
}

export function ProductFormStep(props: ProductFormStepProps) {
  const { children } = props
  return (
    <div className={classes.ProductFormStep}>{children}</div>
  )
}


interface ProductFormStepItemProps {
  finished?: boolean
  children?: React.ReactNode
  onClick: () => void
}

function ProductFormStepItem(props: ProductFormStepItemProps) {
  const { finished, children, onClick } = props
  return (
    <div className={classNames(classes.ProductFormStepItem, {
      [classes.ProductFormStepItemFinished]: finished,
    })} onClick={onClick}>{children} </div>
  )
}

interface ProductStepsProps {
  children: React.ReactNode
  current?: number
  onChange?: (current: number) => void
}


export default function ProductFormSteps(props: ProductStepsProps) {
  const { current = 0, onChange } = props

  function handleStep(index: number) {
    if (_.isFunction(onChange)) {
      onChange(index)
    }
  }

  const children =
    React.Children.map(props.children,
      (element, index) => (
        <ProductFormStepItem finished={current >= index} children={element}
                             onClick={() => handleStep(index)}/>
      ))

  return (
    <div className={classes.ProductFormSteps}>
      {children}
    </div>
  )
}
