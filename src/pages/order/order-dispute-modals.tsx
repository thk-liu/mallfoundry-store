import { Dispute, DisputeKind, OrderService, Refund } from "@mallfoundry/order"
import { Alert, Form, Input, Modal, Typography } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useState } from "react"
import { messageError } from "../../utils/reason"
import classes from "./order-dispute-modals.module.scss"

const { Text } = Typography

function renderDisputeKind(kind?: DisputeKind): string {
  if (kind === DisputeKind.OnlyRefund) {
    return "仅退款"
  } else if (kind === DisputeKind.ReturnRefund) {
    return "退货退款"
  }
  return "未知"
}

interface OrderRefundDisapproveModalProps {
  dispute: Dispute
  visible?: boolean
  onCancel: () => void
  onOk: () => void
}

export function OrderRefundDisapproveModal(props: OrderRefundDisapproveModalProps) {
  const { visible, dispute, onCancel, onOk } = props
  const { orderId, id } = dispute
  const layout = {
    labelCol: { span: 3 },
    wrapperCol: { span: 14 },
  }
  const [form] = Form.useForm()
  const kindText = renderDisputeKind(dispute.kind)
  const [loading, setLoading] = useState(false)

  function onDisapprove() {
    setLoading(true)
    form.validateFields()
      .then(({ disapprovalReason }) =>
        _.assign(new Refund(), {
          id, orderId, disapprovalReason,
        }))
      .then(({ orderId = "", id: disputeId = "" }) =>
        OrderService.disapproveOrderRefund(orderId, disputeId))
      .then(onOk)
      .catch(messageError)
      .finally(() => setLoading(false))
  }

  return (
    <Modal wrapClassName={classes.OrderRefundDisapproveModal}
           visible={visible} width={670} centered title="售后维权处理"
           okText={`拒绝${kindText}`} confirmLoading={loading}
           onCancel={onCancel} onOk={onDisapprove}>
      <Alert type="warning" showIcon
             message="建议你与买家协商后，再确定是否拒绝退款。如你拒绝退款后，买家可修改退款申请协议重新发起退款。"/>
      <Form {...layout} form={form}>
        <Form.Item label="售后方式">
          <span className="ant-form-text">{kindText}</span>
        </Form.Item>
        <Form.Item label="退款金额">
          <span className="ant-form-text">
                <Text type="danger">￥ {dispute.amount}</Text>
          </span>
        </Form.Item>
        <Form.Item name="disapprovalReason" label="拒绝理由" rules={[{ required: true }]}>
          <Input.TextArea placeholder="请填写你的拒绝理由，不超过200个字"
                          autoSize={{ minRows: 3, maxRows: 5 }}/>
        </Form.Item>
      </Form>
    </Modal>
  )
}


interface OrderRefundApproveModalProps {
  dispute: Dispute
  visible?: boolean
  onCancel: () => void
  onOk: () => void
}

export function OrderRefundApproveModal(props: OrderRefundApproveModalProps) {
  const { visible, dispute, onCancel, onOk } = props
  const { orderId = "", id: disputeId = "" } = dispute
  const layout = {
    labelCol: { span: 3 },
    wrapperCol: { span: 14 },
  }
  const kindText = renderDisputeKind(dispute.kind)
  const [loading, setLoading] = useState(false)

  function onApprove() {
    setLoading(true)
    OrderService.approveOrderRefund(orderId, disputeId)
      .then(onOk)
      .catch(messageError)
      .finally(() => setLoading(false))
  }

  return (
    <Modal wrapClassName={classes.OrderRefundApproveModal}
           visible={visible} width={670} centered title="售后维权处理"
           okText={`确认${kindText}`} confirmLoading={loading}
           onCancel={onCancel} onOk={onApprove}>
      <Alert type="warning" showIcon
             message="商家同意后，退款将自动原路退回买家付款账户。"/>
      <Form {...layout}>
        <Form.Item label="售后方式">
          <span className="ant-form-text">{kindText}</span>
        </Form.Item>
        <Form.Item label="退款金额">
          <span className="ant-form-text">
                <Text type="danger">￥ {dispute.amount}</Text>
          </span>
        </Form.Item>
      </Form>
    </Modal>
  )
}
