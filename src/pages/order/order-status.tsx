import { OrderStatus } from "@mallfoundry/order"
import { Steps } from "antd"
import * as React from "react"
import classes from "./order-detail.module.scss"

const { Step } = Steps
const {
  Pending, AwaitingPayment,
  AwaitingFulfillment,
  AwaitingShipment,
  PartiallyShipped,
  Shipped,
  AwaitingPickup,
  Cancelled,
  Completed,
  Declined,
  Closed,
} = OrderStatus

export function useStatusLabel(status: OrderStatus) {
  switch (status) {
    case Pending:
      return "等待付款"
    case AwaitingPayment:
      return "等待付款"
    case AwaitingFulfillment:
      return "等待打包"
    case AwaitingShipment:
      return "等待发货"
    case PartiallyShipped:
      return "部分发货"
    case Shipped:
      return "商家已发货"
    case AwaitingPickup:
      return "等待收货"
    case  Cancelled:
      return "交易取消"
    case  Declined:
      return "交易取消"
    case  Closed:
      return "交易关闭"
    case  Completed:
      return "交易完成"
  }
}

interface StatusLabelProps {
  status: OrderStatus
}

export function StatusLabel(props: StatusLabelProps) {
  return <>{useStatusLabel(props.status)}</>
}

interface OrderStatusStepsProps {
  status: OrderStatus
  placedTime?: string
  paidTime?: string
  shippedTime?: string
  receivedTime?: string
}

export function OrderStatusSteps(props: OrderStatusStepsProps) {
  const { status, placedTime, paidTime, shippedTime, receivedTime } = props

  function statusStep(): number {
    switch (status) {
      case Pending:
      case AwaitingPayment:
        return 0
      case AwaitingFulfillment:
        return 1
      case AwaitingShipment:
      case PartiallyShipped:
      case Shipped:
      case AwaitingPickup:
        return 2
      case Completed:
        return 3
      default :
        return 0
    }
  }

  return (
    <Steps className={classes.orderStatusSteps}
           current={statusStep()}
           size="small"
           labelPlacement="vertical">
      <Step title="买家下单" description={placedTime}/>
      <Step title="买家付款" description={paidTime}/>
      <Step title="商家发货" description={shippedTime}/>
      <Step title="交易成功" description={receivedTime}/>
    </Steps>
  )
}
