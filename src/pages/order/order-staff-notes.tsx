import { Order, OrderService } from "@mallfoundry/order"
import { Input, Modal } from "antd"
import * as React from "react"
import { useEffect, useState } from "react"

const { TextArea } = Input

interface OrderStaffNotesModalProps {
  orderId: string
  staffNotes?: string
  visible: boolean
  onCancel?: () => void
  onSaved?: () => void
}

export function OrderStaffNotesModal(props: OrderStaffNotesModalProps) {
  const { orderId, staffNotes, visible, onCancel, onSaved } = props
  const [textareaValue, setTextareaValue] = useState(staffNotes)
  useEffect(() => setTextareaValue(staffNotes), [staffNotes])
  const [loading, setLoading] = useState(false)

  function onOk() {
    setLoading(true)
    const aOrder = new Order()
    aOrder.id = orderId
    aOrder.staffNotes = textareaValue
    OrderService
      .updateOrder(aOrder)
      .then(onSaved)
      .finally(() => setLoading(false))
  }


  return (
    <Modal title="卖家备注"
           visible={visible}
           keyboard={!loading}
           closable={!loading}
           cancelButtonProps={{
             disabled: loading,
           }}
           onCancel={onCancel}
           confirmLoading={loading}
           onOk={onOk}>
      <TextArea autoSize={{ minRows: 2, maxRows: 6 }}
                value={textareaValue}
                onChange={e => setTextareaValue(e.target.value)}
                maxLength={256}/>
    </Modal>
  )
}
