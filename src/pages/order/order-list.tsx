import StarFilled from "@ant-design/icons/StarFilled"
import { Order, OrderService, OrderStatus } from "@mallfoundry/order"
import { Button, Card, Col, DatePicker, Form, Input, Pagination, Radio, Rate, Row, Select, Space, Spin, Table } from "antd"
import { RadioChangeEvent } from "antd/lib/radio"
import { ColumnProps, TableProps } from "antd/lib/table"
import classNames from "classnames"
import * as dayjs from "dayjs"
import { Dayjs } from "dayjs"
import * as _ from "lodash"
import * as React from "react"
import { useState } from "react"
import { Link, useHistory, useLocation, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useOrders } from "../../hooks/order"
import { OrderDeclineModal } from "./order-decline"
import { useOrderTableItems } from "./order-item"
import classes from "./order-list.module.scss"
import { AddOrderShippingModal, EditOrderShippingModal } from "./order-shipments"
import { SourceName } from "./order-source"
import { OrderStaffNotesModal } from "./order-staff-notes"
import { StatusLabel } from "./order-status"

const { RangePicker } = DatePicker

const ALL_STATUS_VALUES = ""
const AWAITING_PAYMENT_STATUSES = [OrderStatus.Pending, OrderStatus.AwaitingPayment]
const AWAITING_PAYMENT_STATUS_VALUES = AWAITING_PAYMENT_STATUSES.join(",")
const AWAITING_SHIPMENT_STATUSES = [OrderStatus.AwaitingFulfillment, OrderStatus.AwaitingShipment, OrderStatus.PartiallyShipped]
const AWAITING_SHIPMENT_STATUS_VALUES = AWAITING_SHIPMENT_STATUSES.join(",")
const SHIPPED_STATUSES = [OrderStatus.Shipped, OrderStatus.AwaitingPickup]
const SHIPPED_STATUS_VALUES = SHIPPED_STATUSES.join(",")
const COMPLETED_STATUS_VALUES = OrderStatus.Completed
const CANCELLED_STATUS_VALUES = [OrderStatus.Cancelled, OrderStatus.Declined, OrderStatus.Closed].join(",")
const DISPUTED_STATUS_VALUES = OrderStatus.Disputed

class TableOrder {
  public id: string = ""
  public orderId: string = ""
  public productId: string = ""
  public price: number = 0
  public quantity: number = 0
  public imageUrl?: string
  public consignee?: React.ReactNode
  public shippingMethod: string = ""
  public totalAmount: number = 0
  public status?: OrderStatus
  public title: string = ""
  public itemsSize: number = 0
}

interface OrderStaffStarsProps {
  visible: boolean
  staffStars: number
  onSaved?: () => void
  onBlur: () => void
  onFocus: () => void
  onChange: (stars: number) => void
}

function OrderStaffStars(props: OrderStaffStarsProps) {
  const { staffStars, visible, onFocus, onBlur, onChange } = props
  return <>
    <div onMouseEnter={onFocus} onMouseLeave={onBlur} className={classes.orderStaffStars}>
      {
        (!visible && (_.isUndefined(staffStars) || _.isNull(staffStars) || staffStars === 0)) &&
        <Button type="link" size="small">加星</Button>
      }
      {
        (!visible && 0 < staffStars) && <div className={classes.staffStarsCount}>
          <StarFilled/>
          <span className={classes.staffStarsNumbers}> x {staffStars}</span>
        </div>
      }
      {visible && <Rate style={{ fontSize: "15px", height: "24px" }}
                        defaultValue={staffStars}
                        onChange={onChange}/>}
    </div>
  </>
}

const DEFAULT_NAME_COLUMN: ColumnProps<TableOrder> = {
  title: "商品",
  dataIndex: "name",
  className: classes.tableOrderNameColumn,
}
const DEFAULT_PRICE_COLUMN: ColumnProps<TableOrder> = {
  title: "单价(元) / 数量",
  dataIndex: "price",
  width: "140px",
  align: "right",
}
const DEFAULT_CONSIGNEE_COLUMN: ColumnProps<TableOrder> = {
  title: "买家 / 收货人",
  dataIndex: "consignee",
  width: "140px",
  align: "center",
}
const DEFAULT_SHIPPING_METHOD_COLUMN: ColumnProps<TableOrder> = {
  title: "配送方式",
  dataIndex: "shippingMethod",
  width: "140px",
  align: "center",
}
const DEFAULT_TOTAL_AMOUNT_COLUMN: ColumnProps<TableOrder> = {
  title: "实收金额(元)",
  dataIndex: "totalAmount",
  width: "140px",
  align: "center",
}
const DEFAULT_STATUS_COLUMN: ColumnProps<TableOrder> = {
  title: "订单状态",
  dataIndex: "status",
  width: "140px",
  align: "center",
}
const DEFAULT_ACTIONS_COLUMN: ColumnProps<TableOrder> = {
  title: "操作",
  dataIndex: "actions",
  width: "160px",
  align: "center",
}

interface OrderProps {
  order: Order
  onDecline: (order: Order) => void
  onAddShipment: (order: Order) => void
  onEditShipment: (order: Order) => void
  onChangeStaffNotes: (order: Order) => void
}

interface RouterParams {
  storeId: string
}

function OrderCol(props: OrderProps) {
  const { onDecline, onAddShipment, onEditShipment, onChangeStaffNotes } = props
  const { storeId } = useParams<RouterParams>()
  const orderId = props.order.id
  const [loading, setLoading] = useState(false)
  const [staffStarsVisible, setStaffStarsVisible] = useState(false)
  const [staffStars = 0, setStaffStars] = useState(props.order.staffStars)
  const orderTableColumns: ColumnProps<TableOrder>[] = [
    {
      ...DEFAULT_NAME_COLUMN,
      render: (text: string, row) => {
        return <>
          <img src={row.imageUrl} alt=""/>
          <Link to={`/stores/${storeId}/orders/${row.orderId}`}>{text}</Link>
        </>
      },
    },
    {
      ...DEFAULT_PRICE_COLUMN,
      render: (text: string, row) => {
        return <>
          <div>{text}</div>
          <div>{row.quantity}件</div>
        </>
      },
    },
    {
      ...DEFAULT_CONSIGNEE_COLUMN,
      render: (text: string, row: any, index: number) => {
        const obj = {
          children: text,
          props: {
            rowSpan: row.itemsSize,
          },
        }
        if (index !== 0) {
          obj.props.rowSpan = 0
        }
        return obj
      },
    },
    {
      ...DEFAULT_SHIPPING_METHOD_COLUMN,
      render: (text: string, row: TableOrder, index: number) => {
        const obj = {
          children: text,
          props: {
            rowSpan: row.itemsSize,
          },
        }
        if (index !== 0) {
          obj.props.rowSpan = 0
        }
        return obj
      },
    },
    {
      ...DEFAULT_TOTAL_AMOUNT_COLUMN,
      render: (text: string, row: TableOrder, index: number) => {
        const obj = {
          children: text,
          props: {
            rowSpan: row.itemsSize,
          },
        }
        if (index !== 0) {
          obj.props.rowSpan = 0
        }
        return obj
      },
    },
    {
      ...DEFAULT_STATUS_COLUMN,
      render: (text: OrderStatus, row: TableOrder, index: number) => {
        const obj = {
          children: <StatusLabel status={text}/>,
          props: {
            rowSpan: row.itemsSize,
          },
        }
        if (index !== 0) {
          obj.props.rowSpan = 0
        }
        return obj
      },
    },
    {
      ...DEFAULT_ACTIONS_COLUMN,
      render: (text: string, row: TableOrder, index: number) => {
        const obj = {
          children: <>
            {
              _.includes(AWAITING_PAYMENT_STATUSES, row.status)
              && <Button type="link" size="small" onClick={() => onDecline(props.order)}>取消订单</Button>
            }
            {
              _.includes(AWAITING_SHIPMENT_STATUSES, row.status)
              && <Button onClick={() => onAddShipment(props.order)}>发货</Button>
            }
            {
              _.includes(SHIPPED_STATUSES, row.status)
              && <Button type="link" size="small" onClick={() => onEditShipment(props.order)}>修改物流</Button>
            }
          </>,
          props: {
            rowSpan: row.itemsSize,
          },
        }
        if (index !== 0) {
          obj.props.rowSpan = 0
        }
        return obj
      },
    },
  ]

  const dataSource = _.map(props.order.items, item => {
    const { shippingAddress } = props.order
    return _.assign(new TableOrder(), item, {
      orderId,
      productId: item.productId,
      imageUrl: item.imageUrl,
      itemsSize: _.size(props.order.items),
      shippingMethod: "快递",
      status: props.order.status,
      totalAmount: props.order.totalAmount,
      consignee: <>{shippingAddress?.lastName}{shippingAddress?.firstName}<br/>{shippingAddress?.phone}</>,
    })
  })
  const customTableProps: TableProps<any> = {}
  if (!_.isEmpty(props.order.staffNotes)) {
    customTableProps["footer"] = () => `订单备注 : ${props.order.staffNotes}`
  }

  function changeStars(stars: number) {
    const aOrder = new Order()
    aOrder.id = orderId
    aOrder.staffStars = stars
    setLoading(true)
    OrderService
      .updateOrder(aOrder)
      .then(() => setStaffStars(stars))
      .finally(() => setLoading(false))
  }

  return (
    <Table className={classes.order}
           rowKey="id"
           size="small"
           loading={loading}
           title={() => <Row justify="space-between">
             <Col span={18}>
               <Space>
                 <span>订单号：{props.order.id}</span>
                 <span>下单时间：{props.order.placedTime}</span>
                 <SourceName source={props.order.source}/>
               </Space>
             </Col>
             <Col span={6} className={classes.OrderHeaderActions}>
               {
                 !staffStarsVisible
                 && <>
                   <Link to={`/stores/${storeId}/orders/${props.order.id}`}>查看详情</Link>
                   <span className={classes.OrderHeaderSpace}>-</span>
                   <Button type="link" size="small" onClick={() => onChangeStaffNotes(props.order)}>备注</Button>
                   <span className={classes.OrderHeaderSpace}>-</span>
                 </>
               }
               <OrderStaffStars visible={staffStarsVisible}
                                staffStars={staffStars}
                                onFocus={() => setStaffStarsVisible(true)}
                                onBlur={() => setStaffStarsVisible(false)}
                                onChange={changeStars}/>
             </Col>
           </Row>}
           {...customTableProps}
           showHeader={false}
           columns={orderTableColumns}
           dataSource={dataSource}
           bordered
           pagination={false}/>
  )
}

class OrderQueryParams {
  public page?: string | number = 1
  public limit?: string | number = 1
  public ids?: string
  public statuses?: string = ""
  public placedTimes: dayjs.Dayjs[] | undefined[] | string[] = [undefined, undefined]

  public get placedTimeMin() {
    return this.placedTimes[0] ? dayjs(this.placedTimes[0]).format("YYYY-MM-DD") : undefined
  }

  public set placedTimeMin(time: Dayjs | string | undefined) {
    this.placedTimes[0] = time ? dayjs(time) : time
  }

  public get placedTimeMax() {
    return this.placedTimes[1] ? dayjs(this.placedTimes[1]).format("YYYY-MM-DD") : undefined
  }

  public set placedTimeMax(time: Dayjs | string | undefined) {
    this.placedTimes[1] = time ? dayjs(time) : time
  }
}

function useOrderQueryParams(searchParams: URLSearchParams) {
  const queryParams = new OrderQueryParams()
  const page = searchParams.get("page")
  queryParams.page = page ? page : undefined
  const limit = searchParams.get("limit")
  queryParams.limit = limit ? limit : undefined
  const ids = searchParams.get("ids")
  queryParams.ids = ids ? ids : undefined
  const statuses = searchParams.get("statuses")
  queryParams.statuses = statuses ? statuses : ALL_STATUS_VALUES
  const placedTimeMin = searchParams.get("placed_time_min")
  queryParams.placedTimeMin = placedTimeMin ? placedTimeMin : undefined
  const placedTimeMax = searchParams.get("placed_time_max")
  queryParams.placedTimeMax = placedTimeMax ? placedTimeMax : undefined
  return queryParams
}

const ORDER_TABLE_HEADER_COLUMNS: ColumnProps<TableOrder>[] = [
  { ...DEFAULT_NAME_COLUMN },
  { ...DEFAULT_PRICE_COLUMN },
  { ...DEFAULT_CONSIGNEE_COLUMN },
  { ...DEFAULT_SHIPPING_METHOD_COLUMN },
  { ...DEFAULT_TOTAL_AMOUNT_COLUMN },
  { ...DEFAULT_STATUS_COLUMN },
  { ...DEFAULT_ACTIONS_COLUMN },
]

export default function OrderList() {
  const history = useHistory()
  const location = useLocation()
  const { storeId = "" } = useParams<RouterParams>()
  const searchParams = new URLSearchParams(location.search)
  const timestamp = searchParams.get("timestamp") as string
  const queryParams = useOrderQueryParams(searchParams)
  const { page, limit, ids, statuses, placedTimeMin, placedTimeMax } = queryParams
  const [customPlacedTimes, setCustomPlacedTimes] = useState(1)
  const [newLoading, setNewLoading] = useState(false)
  const [form] = Form.useForm()
  const [clickOrder, setClickOrder] = useState(new Order())
  const [declineModalVisible, setDeclineModalVisible] = useState(false)
  const [addShippingModalVisible, setAddShippingModalVisible] = useState(false)
  const [editShippingModalVisible, setEditShippingModalVisible] = useState(false)
  const [staffNotesModalVisible, setStaffNotesModalVisible] = useState(false)

  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
  }

  const {
    loading: ordersLoading,
    elements: orders,
    totalSize,
    refresh: refreshOrders,
  } = useOrders({
    page, limit,
    storeId, ids,
    statuses,
    timestamp,
    placedTimeMin: placedTimeMin as string,
    placedTimeMax: placedTimeMax as string,
  })

  const loading = newLoading || ordersLoading

  function refreshClickOrder() {
    if (!clickOrder.id) {
      return
    }
    setNewLoading(true)
    OrderService.getOrder(clickOrder.id)
      .then(setClickOrder)
      .finally(() => setNewLoading(false))
  }

  function appendSearch(values: any) {
    if (values.page) {
      searchParams.set("page", values.page)
    }
    if (values.limit) {
      searchParams.set("limit", values.limit)
    }
    history.replace(_.assign(location, {
      search: `?${searchParams.toString()}`,
    }))
  }

  function handleSearch() {
    form.validateFields()
      .then(values => {
        if (values.ids) {
          searchParams.set("ids", values.ids)
        } else {
          searchParams.delete("ids")
        }

        if (values.placedTimes) {
          const [minPlacedTime, maxPlacedTime] = values.placedTimes
          if (minPlacedTime) {
            searchParams.set("placed_time_min", dayjs(minPlacedTime).format("YYYY-MM-DD"))
          } else {
            searchParams.delete("placed_time_min")
          }
          if (maxPlacedTime) {
            searchParams.set("placed_time_max", dayjs(maxPlacedTime).format("YYYY-MM-DD"))
          } else {
            searchParams.delete("placed_time_max")
          }
        } else {
          searchParams.delete("placed_time_min")
          searchParams.delete("placed_time_max")
        }

        if (_.isEmpty(values.statuses)) {
          searchParams.delete("statuses")
        } else {
          searchParams.set("statuses", values.statuses)
        }
        searchParams.set("timestamp", Date.now().toString())
        history.replace(_.assign(location, {
          search: `?${searchParams.toString()}`,
        }))
      })
  }

  function handleReset() {
    form.setFieldsValue(new OrderQueryParams())
    setCustomPlacedTimes(1)
    history.push(_.assign(location, {
      search: ``,
    }))
  }

  function handleChangePlacedTimes(e: RadioChangeEvent) {
    const aCustomPlacedTimes = e.target.value
    setCustomPlacedTimes(aCustomPlacedTimes)
    if (aCustomPlacedTimes === 0) {
      form.setFieldsValue({ placedTimes: [dayjs(), dayjs().add(1, "day")] })
    } else if (aCustomPlacedTimes === -1) {
      form.setFieldsValue({ placedTimes: [dayjs().add(-1, "day"), dayjs()] })
    } else if (aCustomPlacedTimes === -7) {
      form.setFieldsValue({ placedTimes: [dayjs().add(-7, "day"), dayjs().add(1, "day")] })
    } else if (aCustomPlacedTimes === -30) {
      form.setFieldsValue({ placedTimes: [dayjs().add(-30, "day"), dayjs().add(1, "day")] })
    }
  }

  function handleSetCustomPlacedTimes(dateStrings: [string, string]) {
    const [dateStart, dateEnd] = dateStrings
    const tomorrow = dayjs().add(1, "day").format("YYYY-MM-DD")
    const today = dayjs().format("YYYY-MM-DD")
    const yesterday = dayjs().add(-1, "day").format("YYYY-MM-DD")
    const in7day = dayjs().add(-7, "day").format("YYYY-MM-DD")
    const in30day = dayjs().add(-30, "day").format("YYYY-MM-DD")
    if (dateStart === today && dateEnd === tomorrow) {
      setCustomPlacedTimes(0)
    } else if (dateStart === yesterday && dateEnd === today) {
      setCustomPlacedTimes(-1)
    } else if (dateStart === in7day && dateEnd === tomorrow) {
      setCustomPlacedTimes(-7)
    } else if (dateStart === in30day && dateEnd === tomorrow) {
      setCustomPlacedTimes(-30)
    } else {
      setCustomPlacedTimes(1)
    }
  }

  const tableItems = useOrderTableItems(clickOrder)

  return (
    <div className={classes.orderList}>
      <Page.Header title="订单管理" ghost={false}/>
      <Page.Content>
        <Card>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Card className={classes.filterBar} bordered={false}>
                <Form {...layout} form={form} initialValues={queryParams}>
                  <Row>
                    <Col span={6}>
                      <Form.Item name="ids" label="订单编号">
                        <Input placeholder="请输入订单编号" allowClear/>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="types" label="订单类型">
                        <Select placeholder="请选择类型">
                          <Select.Option value="">全部</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="after_services" label="售后状态">
                        <Select placeholder="请选择状态">
                          <Select.Option value="male">全部</Select.Option>
                          <Select.Option value="female">待发货</Select.Option>
                          <Select.Option value="other">已收货</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Form.Item label="下单时间" name="placedTimes">
                        <RangePicker style={{ width: "100%" }} onChange={(dates, dateStrings) => {
                          handleSetCustomPlacedTimes(dateStrings)
                        }}/>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Radio.Group style={{ marginLeft: "8px" }} value={customPlacedTimes} onChange={handleChangePlacedTimes}>
                        <Space>
                          <Radio.Button value={0} className={classes.placedDateButton}>今</Radio.Button>
                          <Radio.Button value={-1} className={classes.placedDateButton}>昨</Radio.Button>
                          <Radio.Button value={-7} className={classes.placedDateButton}>近7天</Radio.Button>
                          <Radio.Button value={-30} className={classes.placedDateButton}>近30天</Radio.Button>
                        </Space>
                      </Radio.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Form.Item name="statuses" label="订单状态">
                        <Select placeholder="请选择订单状态">
                          <Select.Option value={ALL_STATUS_VALUES}>全部</Select.Option>
                          <Select.Option value={AWAITING_PAYMENT_STATUS_VALUES}>待付款</Select.Option>
                          <Select.Option value={AWAITING_SHIPMENT_STATUS_VALUES}>待发货</Select.Option>
                          <Select.Option value={SHIPPED_STATUS_VALUES}>已发货</Select.Option>
                          <Select.Option value={COMPLETED_STATUS_VALUES}>已完成</Select.Option>
                          <Select.Option value={CANCELLED_STATUS_VALUES}>已关闭</Select.Option>
                          <Select.Option value={DISPUTED_STATUS_VALUES}>退款中</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="shipping_methods" label="配送方式">
                        <Select placeholder="请选择配送方式">
                          <Select.Option value="">全部</Select.Option>
                          <Select.Option value="female">快递</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      <Form.Item name="payment_methods" label="付款方式">
                        <Select placeholder="请选择付款方式">
                          <Select.Option value="">全部</Select.Option>
                          <Select.Option value="alipay">支付宝</Select.Option>
                          <Select.Option value="wxpay">微信支付</Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6}>
                      <Row>
                        <Col offset={6}>
                          <Space>
                            <Button type="primary" onClick={handleSearch} loading={loading}>筛选</Button>
                            <Button onClick={handleReset}>重置</Button>
                          </Space>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Form>
              </Card>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col>
              <Radio.Group value={statuses} onChange={e => {
                if (e.target.value) {
                  searchParams.set("statuses", e.target.value)
                } else {
                  searchParams.delete("statuses")
                }
                form.setFieldsValue({ statuses: e.target.value })
                history.push(_.assign(location, {
                  search: `?${searchParams.toString()}`,
                }))
              }}>
                <Radio.Button value={ALL_STATUS_VALUES}>全部</Radio.Button>
                <Radio.Button value={AWAITING_PAYMENT_STATUS_VALUES}>待付款</Radio.Button>
                <Radio.Button value={AWAITING_SHIPMENT_STATUS_VALUES}>待发货</Radio.Button>
                <Radio.Button value={SHIPPED_STATUS_VALUES}>已发货</Radio.Button>
                <Radio.Button value={COMPLETED_STATUS_VALUES}>已完成</Radio.Button>
                <Radio.Button value={CANCELLED_STATUS_VALUES}>已关闭</Radio.Button>
                <Radio.Button value={DISPUTED_STATUS_VALUES}>退款中</Radio.Button>
              </Radio.Group>
            </Col>
          </Row>
          <OrderStaffNotesModal
            visible={staffNotesModalVisible}
            orderId={_.isUndefined(clickOrder.id) ? "" : clickOrder.id}
            staffNotes={clickOrder.staffNotes}
            onCancel={() => setStaffNotesModalVisible(false)}
            onSaved={() => {
              setStaffNotesModalVisible(false)
              refreshClickOrder()
              refreshOrders()
            }}/>
          <OrderDeclineModal
            orderId={_.isUndefined(clickOrder.id) ? "" : clickOrder.id}
            visible={declineModalVisible}
            onCancel={() => setDeclineModalVisible(false)}
            onDeclined={() => {
              setDeclineModalVisible(false)
              refreshOrders()
            }}/>
          <AddOrderShippingModal
            visible={addShippingModalVisible && _.includes(AWAITING_SHIPMENT_STATUSES, clickOrder.status)}
            orderId={_.isUndefined(clickOrder.id) ? "" : clickOrder.id}
            shippingAddress={clickOrder.shippingAddress}
            items={tableItems}
            onCancel={() => setAddShippingModalVisible(false)}
            onShipped={() => {
              refreshClickOrder()
              refreshOrders()
            }}/>
          <EditOrderShippingModal
            visible={editShippingModalVisible}
            orderId={_.isUndefined(clickOrder.id) ? "" : clickOrder.id}
            shipments={clickOrder.shipments}
            onCancel={() => setEditShippingModalVisible(false)}
            onSaved={() => {
              setEditShippingModalVisible(false)
              refreshClickOrder()
              refreshOrders()
            }}/>
          <Row>
            <Col span={24}>
              <Spin spinning={loading}>
                <Table className={classNames({
                  [classes.orderTableHeader]: !_.isEmpty(orders),
                })} columns={ORDER_TABLE_HEADER_COLUMNS} pagination={false}/>
                {
                  _.map(orders, order => (
                    <OrderCol key={order.id} order={order}
                              onDecline={aOrder => {
                                setClickOrder(aOrder)
                                setDeclineModalVisible(true)
                              }}
                              onAddShipment={aOrder => {
                                setClickOrder(aOrder)
                                setAddShippingModalVisible(true)
                              }}
                              onEditShipment={aOrder => {
                                setClickOrder(aOrder)
                                setEditShippingModalVisible(true)
                              }}
                              onChangeStaffNotes={aOrder => {
                                setClickOrder(aOrder)
                                setStaffNotesModalVisible(true)
                              }}/>
                  ))
                }
              </Spin>
            </Col>
          </Row>
          {!_.isEmpty(orders)
          && <Row justify="end">
            <Col>
              <Pagination current={_.toNumber(page)}
                          pageSize={_.toNumber(limit)}
                          showSizeChanger
                          pageSizeOptions={["10", "20", "50", "100"]}
                          total={totalSize}
                          showTotal={total => `共 ${total} 条`}
                          onChange={(page, pageSize) => appendSearch({ page, limit: pageSize })}/>
            </Col>
          </Row>}
        </Card>
      </Page.Content>
    </div>
  )
}
