import { Transaction, TransactionStatus, TransactionType } from "@mallfoundry/finance"
import { Button, Table, Typography } from "antd"
import { ColumnProps } from "antd/lib/table"
import * as _ from "lodash"
import * as React from "react"

const { Text } = Typography

function renderType(type?: TransactionType) {
  if (type === TransactionType.Payment) {
    return "支付"
  } else if (type === TransactionType.PaymentRefund) {
    return "退款"
  } else if (type === TransactionType.Topup) {
    return "充值"
  } else if (type === TransactionType.Withdrawal) {
    return "提现"
  }
  return type
}

function statusToLabel(status?: TransactionStatus, type?: TransactionType) {
  if (status === TransactionStatus.Pending && type === TransactionType.Withdrawal) {
    return "申请中"
  }
  if (status === TransactionStatus.Pending && type === TransactionType.Topup) {
    return "充值中"
  }
  if (status === TransactionStatus.Pending && type === TransactionType.Payment) {
    return "付款中"
  }
  if (status === TransactionStatus.Canceled) {
    return "已取消"
  }
  if (status === TransactionStatus.Disapproved) {
    return "未批准"
  }
  if (status === TransactionStatus.Processing) {
    return "处理中"
  }
  if (status === TransactionStatus.Succeeded) {
    return "成功"
  }
  if (status === TransactionStatus.Failed) {
    return "失败"
  }
  return status
}

function renderStatus(status?: TransactionStatus, type?: TransactionType) {
  const statusLabel = statusToLabel(status, type)
  if (status === TransactionStatus.Disapproved || status === TransactionStatus.Failed) {
    return <Text type="danger">{statusLabel}</Text>
  } else if (status === TransactionStatus.Succeeded) {
    return <Text type="success">{statusLabel}</Text>
  }
  return statusLabel
}


const columns: ColumnProps<Transaction>[] = [
  {
    title: "流水号",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "时间",
    dataIndex: "createdTime",
    key: "createdTime",
  },
  {
    title: "名称",
    dataIndex: "name",
    key: "name",
    render: (name: string, { type }) => {
      if (name && type === TransactionType.Payment) {
        return name
      }
      return renderType(type)
    },
  },
  {
    title: "对方",
    dataIndex: "address",
    key: "address",
    render: () => "-",
  },
  {
    title: "金额（元）",
    dataIndex: "amount",
    key: "amount",
    render: amount => _.toNumber(amount).toFixed(2),
  },
  {
    title: "状态",
    dataIndex: "status",
    key: "status",
    align: "center",
    render: (status: TransactionStatus, tx) => renderStatus(status, tx.type),
  },
  {
    title: "类型",
    dataIndex: "type",
    key: "type",
    align: "center",
    render: renderType,
  },
  {
    title: "操作",
    dataIndex: "key",
    key: "key",
    width: "100px",
    align: "right",
    render: () => <Button type="link" size="small" style={{ padding: "0" }}>查看</Button>,
  },
]

interface TransactionTableProps {
  loading?: boolean
  dataSource?: Transaction[]
  page?: string | number
  limit?: string | number
}

export default function TransactionTable(props: TransactionTableProps) {
  const { loading, dataSource } = props
  return <Table rowKey="id" loading={loading}
                columns={columns} dataSource={dataSource}
  />
}
