import { WithdrawalStatus } from "@mallfoundry/finance"
import { Button, Card, Col, Row, Space, Steps, Typography } from "antd"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useWithdrawal } from "../../hooks/finance"
import classes from "./withdrawal-progress.module.scss"


const { Title } = Typography

interface WithdrawalStatusTitleProps {
  status?: WithdrawalStatus
}

function WithdrawalStatusTitle({ status }: WithdrawalStatusTitleProps) {
  if (WithdrawalStatus.Pending === status) {
    return <>提现申请中</>
  } else if (WithdrawalStatus.Disapproved === status) {
    return <>提现未批准</>
  } else if (WithdrawalStatus.Canceled === status) {
    return <>提现已取消</>
  } else if (WithdrawalStatus.Processing === status) {
    return <>提现处理中</>
  } else if (WithdrawalStatus.Succeeded === status) {
    return <>提现成功</>
  } else if (WithdrawalStatus.Failed === status) {
    return <>提现失败</>
  }
  return <></>
}


export default function WithdrawalProgress() {
  const { storeId, withdrawalId } = useParams<{ storeId: string, withdrawalId: string }>()
  const { loading, withdrawal } = useWithdrawal(withdrawalId)

  return (
    <div className={classes.WithdrawalProgress}>
      <Page.Header title="提现状态" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Title level={5}>
                <WithdrawalStatusTitle status={withdrawal.status}/>
              </Title>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Steps className={classes.WithdrawalProgressSteps}
                     size="small" direction="vertical" current={1}>
                <Steps.Step title="提现申请已提交，等待银行处理" description="1.00元 中国工商银行（2342）"/>
                <Steps.Step title="根据不同银行规定，预计在1-3个工作日内到账"/>
              </Steps>
            </Col>
          </Row>
          <Row justify="center">
            <Col>
              <Space>
                <Button>
                  <Link to={`/stores/${storeId}/withdrawals`}>返回提现记录</Link>
                </Button>
                <Button type="primary">
                  <Link to={`/stores/${storeId}/withdrawals/apply`}>继续提现</Link>
                </Button>
              </Space>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
