import { HolderType, Recipient, Withdrawal, WithdrawalService, WithdrawalStatus } from "@mallfoundry/finance"
import { Button, Card, Col, DatePicker, Divider, Form, Popconfirm, Radio, Row, Select, Space, Table, Tabs, Typography } from "antd"
import { RadioChangeEvent } from "antd/lib/radio"
import { ColumnProps } from "antd/lib/table"
import * as dayjs from "dayjs"
import * as _ from "lodash"
import * as React from "react"
import { useEffect, useState } from "react"
import { Link, useHistory, useLocation, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useWithdrawals } from "../../hooks/finance"
import { useLoading } from "../../hooks/loading"
import { useStore } from "../../hooks/store"
import classes from "./withdrawal-list.module.scss"


const { Text, Link: TextLink } = Typography
const { TabPane } = Tabs
const { RangePicker } = DatePicker

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
}


function statusToLabel(status: WithdrawalStatus) {
  if (status === WithdrawalStatus.Pending) {
    return "申请中"
  } else if (status === WithdrawalStatus.Canceled) {
    return "已取消"
  } else if (status === WithdrawalStatus.Disapproved) {
    return "未批准"
  } else if (status === WithdrawalStatus.Processing) {
    return "处理中"
  } else if (status === WithdrawalStatus.Succeeded) {
    return "提现成功"
  } else if (status === WithdrawalStatus.Failed) {
    return "提现失败"
  }
}

function renderStatus(status: WithdrawalStatus) {
  const statusLabel = statusToLabel(status)
  if (status === WithdrawalStatus.Disapproved || status === WithdrawalStatus.Failed) {
    return <Text type="danger">{statusLabel}</Text>
  } else if (status === WithdrawalStatus.Succeeded) {
    return <Text type="success">{statusLabel}</Text>
  }
  return statusLabel
}

function renderRecipient(recipient: Recipient) {
  const { bankName, holderName, holderType, number } = recipient
  const recipients = [bankName, holderName]

  const endRecipients = []
  const numberSize = _.size(number)
  if (numberSize > 4) {
    const last4 = number?.substring(numberSize - 4)
    endRecipients.push(`尾号${last4}`)
  }

  if (holderType === HolderType.Individual) {
    endRecipients.push("（私）")
  } else if (holderType === HolderType.Company) {
    endRecipients.push("（公）")
  }
  recipients.push(_.join(endRecipients, ""))
  return _.join(recipients, "-")
}

const BASE_COLUMNS: ColumnProps<Withdrawal>[] = [
  {
    title: "流水号",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "提现金额（元）",
    dataIndex: "amount",
    key: "amount",
    align: "right",
    render: amount => _.toNumber(amount).toFixed(2),
  },
  {
    title: "到账银行卡",
    dataIndex: "recipient",
    key: "recipient",
    render: renderRecipient,
  },
  {
    title: "提现状态",
    dataIndex: "status",
    key: "status",
    align: "center",
    render: renderStatus,
  },
  {
    title: "提现备注",
    dataIndex: "memo",
    key: "memo",
    render: memo => _.isEmpty(memo) ? "-" : memo,
  },
  {
    title: "操作人",
    dataIndex: "operator",
    key: "operator",
    width: "160px",
  },
  {
    title: "申请时间",
    dataIndex: "appliedTime",
    key: "appliedTime",
    width: "160px",
  },

]

interface WithdrawalCancelButtonProps {
  withdrawalId: string
  onCanceled: () => void
}

function WithdrawalCancelButton({ withdrawalId, onCanceled }: WithdrawalCancelButtonProps) {
  const [cancelLoading, setCancelLoading] = useState(false)
  const [cancelVisible, setCancelVisible] = useState(false)

  function handleCancel() {
    setCancelLoading(true)
    WithdrawalService
      .cancelWithdrawal(withdrawalId)
      .then(onCanceled)
      .finally(() => setCancelLoading(false))
  }

  return <Popconfirm visible={cancelVisible || cancelLoading}
                     placement="bottomRight" title="您确定要取消吗？"
                     okButtonProps={{ loading: cancelLoading }}
                     onConfirm={handleCancel}
                     onVisibleChange={setCancelVisible}>
    <TextLink onClick={() => setCancelVisible(!cancelVisible)}>取消</TextLink>
  </Popconfirm>
}


export default function WithdrawalList() {
  const history = useHistory()
  const location = useLocation()
  const { storeId } = useParams<{ storeId: string }>()
  const searchParams = new URLSearchParams(location.search)
  const page = searchParams.get("page") ?? 1
  const limit = searchParams.get("limit") ?? 20
  const statuses = searchParams.get("statuses") ?? ""
  const appliedTimeStart = searchParams.get("applied_time_start") ?? ""
  const appliedTimeEnd = searchParams.get("applied_time_end") ?? ""

  const [customAppliedTimes, setCustomAppliedTimes] = useState(0)

  const [form] = Form.useForm()

  const { loading: storeLoading, store: { accountId } } = useStore(storeId)

  const {
    loading: withdrawalsLoading,
    refresh: refreshWithdrawals,
    elements: withdrawals,
  } = useWithdrawals({
    page,
    limit,
    accountId,
    statuses,
    appliedTimeStart,
    appliedTimeEnd,
  })

  const [loading] = useLoading([storeLoading, withdrawalsLoading])

  const columns: ColumnProps<Withdrawal>[] = [...BASE_COLUMNS, {
    title: "操作",
    dataIndex: "id",
    key: "id",
    align: "right",
    width: "140px",
    render: (id: string, { status }: Withdrawal) => <Space align="center">
      <Link to={`/stores/${storeId}/withdrawals/${id}/progress`}>查看</Link>
      {status === WithdrawalStatus.Pending && <>
        <Divider type="vertical" style={{ margin: 0 }}/>
        <WithdrawalCancelButton withdrawalId={id} onCanceled={refreshWithdrawals}/>
      </>}
    </Space>,
  }]


  useEffect(() => {
    form.setFieldsValue({
      statuses,
      appliedTimes: [
        appliedTimeStart ? dayjs(appliedTimeStart) : undefined,
        appliedTimeEnd ? dayjs(appliedTimeEnd) : undefined,
      ],
    })
    handleSetCustomAppliedTimes([appliedTimeStart, appliedTimeEnd])
  }, [form, statuses, appliedTimeStart, appliedTimeEnd])

  function handleChangeAppliedTimes(e: RadioChangeEvent) {
    const aCustomAppliedTimes = e.target.value
    setCustomAppliedTimes(aCustomAppliedTimes)
    if (aCustomAppliedTimes === -7) {
      form.setFieldsValue({ appliedTimes: [dayjs().add(-6, "day"), dayjs()] })
    } else if (aCustomAppliedTimes === -30) {
      form.setFieldsValue({ appliedTimes: [dayjs().add(-29, "day"), dayjs()] })
    }
  }

  function handleSetCustomAppliedTimes(dateStrings: [string, string]) {
    const [dateStart, dateEnd] = dateStrings
    const today = dayjs().format("YYYY-MM-DD")
    const in7day = dayjs().add(-6, "day").format("YYYY-MM-DD")
    const in30day = dayjs().add(-29, "day").format("YYYY-MM-DD")
    if (dateStart === in7day && dateEnd === today) {
      setCustomAppliedTimes(-7)
    } else if (dateStart === in30day && dateEnd === today) {
      setCustomAppliedTimes(-30)
    } else {
      setCustomAppliedTimes(0)
    }
  }

  function handleSearch() {
    form.validateFields()
      .then(values => {
        const { statuses, appliedTimes } = values
        if (statuses) {
          searchParams.set("statuses", statuses)
        } else {
          searchParams.delete("statuses")
        }
        if (appliedTimes) {
          const [appliedTimeStart, appliedTimeEnd] = appliedTimes
          if (appliedTimeStart) {
            searchParams.set("applied_time_start", dayjs(appliedTimeStart).format("YYYY-MM-DD"))
          } else {
            searchParams.delete("applied_time_start")
          }
          if (appliedTimeEnd) {
            searchParams.set("applied_time_end", dayjs(appliedTimeEnd).format("YYYY-MM-DD"))
          } else {
            searchParams.delete("applied_time_end")
          }
        } else {
          searchParams.delete("applied_time_start")
          searchParams.delete("applied_time_end")
        }
        history.replace(_.assign(location, {
          search: `?${searchParams.toString()}`,
        }))
      })
  }

  function handleReset() {
    history.push(_.assign(location, {
      search: ``,
    }))
  }

  return (
    <div className={classes.WithdrawalList}>
      <Page.Header title="提现记录" ghost={false}/>
      <Page.Content>
        <Card>
          <Tabs activeKey="2" animated={false} onChange={(tabKey: string) => {
            if (tabKey === "1") {
              history.push(`/stores/${storeId}/withdrawals/apply`)
            }
          }}>
            <TabPane tab="提现" key="1"/>
            <TabPane tab="提现记录" key="2">
              <Row gutter={[16, 16]}>
                <Col span={24}>
                  <Card className={classes.FilterBar} bordered={false}>
                    <Form {...layout} form={form}>
                      <Row>
                        <Col span={6}>
                          <Form.Item label="申请时间" name="appliedTimes">
                            <RangePicker style={{ width: "100%" }}
                                         onChange={(dates, dateStrings) => handleSetCustomAppliedTimes(dateStrings)}/>
                          </Form.Item>
                        </Col>
                        <Col span={6}>
                          <Radio.Group style={{ marginLeft: "8px" }} value={customAppliedTimes} onChange={handleChangeAppliedTimes}>
                            <Space>
                              <Radio.Button value={-7} className={classes.PlacedDateButton}>近7天</Radio.Button>
                              <Radio.Button value={-30} className={classes.PlacedDateButton}>近30天</Radio.Button>
                            </Space>
                          </Radio.Group>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={6}>
                          <Form.Item name="statuses" label="提现状态">
                            <Select>
                              <Select.Option value="">全部</Select.Option>
                              <Select.Option value="pending">提现申请中</Select.Option>
                              <Select.Option value="processing">提现处理中</Select.Option>
                              <Select.Option value="disapproved">提现异常</Select.Option>
                              <Select.Option value="succeeded">提现成功</Select.Option>
                              <Select.Option value="failed">提现失败</Select.Option>
                              <Select.Option value="canceled">提现取消</Select.Option>
                            </Select>
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={6}>
                          <Row>
                            <Col offset={6}>
                              <Space>
                                <Button type="primary" onClick={handleSearch} loading={loading}>筛选</Button>
                                <Button onClick={handleReset}>重置</Button>
                              </Space>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  </Card>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Table rowKey="id" loading={loading} columns={columns} dataSource={withdrawals}/>
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Card>
      </Page.Content>
    </div>
  )
}
