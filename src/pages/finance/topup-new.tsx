import { CurrencyCode, Topup, TopupService } from "@mallfoundry/finance"
import { Button, Card, Form, InputNumber, Radio, Skeleton, Tabs, Typography } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useAccount, useBalance } from "../../hooks/finance"
import { useStore } from "../../hooks/store"
import { messageError } from "../../utils/reason"
import classes from "./topup-new.module.scss"
import { TopupPayModal } from "./topup-pay"


const { Text } = Typography
const { TabPane } = Tabs

const layout = {
  labelCol: { span: 2 },
  wrapperCol: { span: 4 },
}

const tailLayout = {
  wrapperCol: { offset: 2, span: 4 },
}

export default function TopupNew() {
  const history = useHistory()
  const { storeId } = useParams<{ storeId: string }>()
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [payModalVisible, setPayModalVisible] = useState(false)
  const { loading: storeLoading, store: { accountId } } = useStore(storeId)
  const { loading: accountLoading, account } = useAccount(accountId)
  const { loading: balanceLoading, balance } = useBalance(accountId, CurrencyCode.CNY)
  const loading = storeLoading || accountLoading || balanceLoading
  console.log(setPayModalVisible)

  function handleNew(values: any) {
    const { amount } = values

    const topup = _.assign(new Topup(), {
      accountId, currency: CurrencyCode.CNY, amount,
    })

    setConfirmLoading(true)
    TopupService.createTopup(topup)
      .catch(reason => {
        messageError(reason)
        setConfirmLoading(false)
      })
      .finally(() => setConfirmLoading(false))
  }

  return (
    <div className={classes.TopupNew}>
      <Page.Header title="充值" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Tabs activeKey="1" animated={false} onChange={tabKey => {
            if (tabKey === "2") {
              history.push(`/stores/${storeId}/topups`)
            }
          }}>
            <TabPane tab="充值" key="1">
              <TopupPayModal visible={false} storeId={storeId} amount="20.02"/>
              <Form name="topup-new-form" {...layout} initialValues={{ amount: 1 }} onFinish={handleNew}>
                <Form.Item label="账户名称">
                  {loading ? <Skeleton.Input style={{ width: "160px" }} active/> : <span className="ant-form-text">{account.name}</span>}
                </Form.Item>
                <Form.Item required label="收款账户">
                  <Radio checked>店铺余额</Radio>
                </Form.Item>
                <Form.Item label="账户可用余额">
                  {loading ? <Skeleton.Input style={{ width: "160px" }} active/> :
                    <Text className="ant-form-text" type="danger">{balance.availableAmount} 元</Text>}
                </Form.Item>
                <Form.Item label="充值金额" name="amount" required>
                  <InputNumber style={{ width: "160px" }} min={0.01} precision={2}/>
                </Form.Item>
                <Form.Item {...tailLayout}>
                  <Button type="primary" htmlType="submit" loading={confirmLoading || payModalVisible}>确定充值</Button>
                </Form.Item>
              </Form>
            </TabPane>
            <TabPane tab="充值记录" key="2"/>
          </Tabs>
        </Card>
      </Page.Content>
    </div>
  )
}
