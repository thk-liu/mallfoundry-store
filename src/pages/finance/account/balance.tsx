import QuestionCircleFilled from "@ant-design/icons/QuestionCircleFilled"
import { CurrencyCode } from "@mallfoundry/finance"
import { Button, Card, Col, Radio, Row, Skeleton, Space } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import BlockHeader from "../../../components/block-header"
import Page from "../../../components/page"
import { useBalance, useTransactions } from "../../../hooks/finance"
import { useStore } from "../../../hooks/store"
import TransactionTable from "../transaction-table"
import classes from "./balance.module.scss"

interface BalanceAmountProps {
  title?: React.ReactNode
  value?: number
  children?: React.ReactNode
}

function BalanceAmount(props: BalanceAmountProps) {
  const { title, value, children } = props
  const amount = _.toNumber(value).toFixed(2)
  return (
    <div className={classes.BalanceAmount}>
      <div className={classes.BalanceAmountTitle}>
        {title}
      </div>
      <div className={classes.BalanceAmountContent}>
        <span className={classes.BalanceAmountValue}>{amount}</span>
        {children}
      </div>
    </div>
  )
}


export default function Balance() {
  const { storeId } = useParams<{ storeId: string }>()
  const { loading: storeLoading, store } = useStore(storeId)
  const { accountId } = store
  const { loading: balanceLoading, balance } = useBalance(accountId, CurrencyCode.CNY)
  const { pendingAmount, availableAmount, freezeAmount } = balance
  const {
    loading: transactionsLoading,
    elements: transactions,
  } = useTransactions({
    accountId,
  })

  return (
    <div className={classes.Balance}>
      <Page.Header title="店铺余额" ghost={false}/>
      <Page.Content>
        <Card>
          <BlockHeader title="店铺余额"/>
          <Skeleton active loading={storeLoading || balanceLoading} title={false} paragraph={{ rows: 4, width: "100%" }}>
            <Row className={classes.BalanceAmounts} justify="space-between">
              <Col>
                <BalanceAmount title={<>可用店铺余额(元)<QuestionCircleFilled/></>} value={availableAmount}>
                  <Space style={{ marginLeft: "24px" }}>
                    <Button type="primary">
                      <Link to={`/stores/${storeId}/topups/new`}>充值</Link>
                    </Button>
                    <Button>
                      <Link to={`/stores/${storeId}/withdrawals/apply`}>提现</Link>
                    </Button>
                  </Space>
                </BalanceAmount>
              </Col>
              <Col>
                <BalanceAmount title={<>待结算(元)<QuestionCircleFilled/></>} value={pendingAmount}/>
              </Col>
              <Col>
                <BalanceAmount title={<>不可用余额(元)<QuestionCircleFilled/></>} value={freezeAmount}/>
              </Col>
            </Row>
          </Skeleton>
          <BlockHeader title="最近交易记录" justify="space-between">
            <Link to={`/stores/${storeId}/transactions`}>全部交易记录</Link>
          </BlockHeader>
          <Row gutter={[16, 16]}>
            <Col>
              <Radio.Group>
                <Radio.Button>全部</Radio.Button>
                <Radio.Button value={1}>进行中</Radio.Button>
                <Radio.Button value={2}>待结算</Radio.Button>
                <Radio.Button value={3}>退款</Radio.Button>
                <Radio.Button value={4}>交易关闭</Radio.Button>
                <Radio.Button value={5}>交易完成</Radio.Button>
              </Radio.Group>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <TransactionTable loading={storeLoading || transactionsLoading} dataSource={transactions}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
