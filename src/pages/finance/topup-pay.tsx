import { Button, Form, Modal, Skeleton, Statistic } from "antd"
import * as React from "react"
import { useAccount } from "../../hooks/finance"
import { useStore } from "../../hooks/store"
import classes from "./topup-pay.module.scss"

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 12 },
}


interface TopupPayModalProps {
  visible?: boolean
  storeId: string
  rechargeId?: string
  amount: string | number
}

export function TopupPayModal(props: TopupPayModalProps) {
  const { visible, storeId, amount } = props
  const { loading: storeLoading, store } = useStore(storeId)
  const { accountId } = store
  const { loading: accountLoading, account } = useAccount(accountId)
  // const loading = storeLoading || accountLoading
  return <Modal visible={visible} title="收银台" centered
                className={classes.TopupPayModal}
                footer={<Button type="primary">已完成支付</Button>}>
    <Form name="recharge-new-form" {...layout} initialValues={{ amount: 1 }}>
      <Form.Item label="商家名称">
        {storeLoading ? <Skeleton.Input style={{ width: "160px" }} active/> : <span className="ant-form-text">{store.name}</span>}
      </Form.Item>
      <Form.Item label="账户名称">
        {accountLoading ? <Skeleton.Input style={{ width: "160px" }} active/> : <span className="ant-form-text">{account.name}</span>}
      </Form.Item>
      <Form.Item label="充值金额" name="amount">
        <Statistic value={amount} precision={2} suffix="元"/>
      </Form.Item>
    </Form>
  </Modal>
}
