import { Area } from "@ant-design/charts"
import InfoCircleOutlined from "@ant-design/icons/InfoCircleOutlined"
import { DailyPage, PageQuery, PageReport, TotalPages } from "@mallfoundry/report"
import { Card, Col, Divider, Row, Space, Statistic, Typography } from "antd"
import * as dayjs from "dayjs"
import * as _ from "lodash"
import * as React from "react"
import { useEffect, useState } from "react"
import classes from "./monthly-analysis.module.scss"

const { Text } = Typography

interface MonthlyPagesAreaProps {
  storeId: string
}

const DATE_FIELD_NAME = "时间"
const VIEW_COUNT_FIELD_NAME = "访问量"

function MonthlyPagesArea(props: MonthlyPagesAreaProps) {
  const { storeId } = props
  const [dailyPages, setDailyPages] = useState([] as DailyPage[])
  const asyncFetch = () => {
    // const dateStart = dayjs().date(1).format("YYYYMMDD")
    // const dateEnd = dayjs().add(1, "month").date(0).format("YYYYMMDD")
    const dateStart = "20200901"
    const dateEnd = "20200930"
    PageReport
      .queryDailyPages(
        new PageQuery().toBuilder().storeId(storeId).dateFrom(dateStart).dateTo(dateEnd).build(),
      )
      .then(pages => _.map(pages, page => _.assign(new DailyPage(), page, {
        [DATE_FIELD_NAME]: dayjs(page.date, "YYYYMMDD").format("YYYY-MM-DD"),
        [VIEW_COUNT_FIELD_NAME]: page.viewCount,
      })))
      .then(setDailyPages)
  }
  useEffect(asyncFetch, [storeId])
  const config = {
    height: 60,
    forceFit: true,
    smooth: true,
    padding: [12, 0, 12, 0],
    data: dailyPages,
    xField: DATE_FIELD_NAME,
    yField: VIEW_COUNT_FIELD_NAME,
    xAxis: {
      line: null,
      label: null,
    },
    yAxis: {
      label: null,
      grid: null,
    },
    line: {
      visible: false,
    },
    tooltip: {
      fields: [DATE_FIELD_NAME, VIEW_COUNT_FIELD_NAME],
      showTitle: false,
    },
  }
  return <Area {...config} />
}

interface MonthlyPagesProps {
  storeId: string
}

export default function MonthlyPages(props: MonthlyPagesProps) {
  const { storeId } = props
  const [monthlyTotalPages, setMonthlyTotalPages] = useState(new TotalPages())
  const [dailyTotalPages, setDailyTotalPages] = useState(new TotalPages())

  useEffect(() => {
    PageReport.queryTotalPages(
      new PageQuery().toBuilder().storeId(storeId).build())
      .then(setMonthlyTotalPages)
  }, [storeId])

  useEffect(() => {
    PageReport.queryTotalPages(
      new PageQuery().toBuilder().storeId(storeId).build())
      .then(setDailyTotalPages)
  }, [storeId])

  return (
    <Card size="small">
      <Row justify="space-between">
        <Col>
          <Statistic title="访问量" className={classes.MonthlyTotalStatistic} value={monthlyTotalPages.viewCount}/>
        </Col>
        <Col>
          <Text type="secondary">
            <InfoCircleOutlined/>
          </Text>
        </Col>
      </Row>
      <MonthlyPagesArea storeId={storeId}/>
      <Divider/>
      <Space>
        <Text>日访问量</Text>
        <Text>{dailyTotalPages.viewCount}</Text>
      </Space>
    </Card>
  )
}
