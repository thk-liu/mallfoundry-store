import CaretDownOutlined from "@ant-design/icons/CaretDownOutlined"
import CaretUpOutlined from "@ant-design/icons/CaretUpOutlined"
import InfoCircleOutlined from "@ant-design/icons/InfoCircleOutlined"
import { DailySale, SalesQuery, SalesReport, TotalSales } from "@mallfoundry/report"
import { Card, Col, Divider, Row, Space, Statistic, Typography } from "antd"
import * as React from "react"
import { useEffect, useState } from "react"
import Trend from "../../components/trend"
import classes from "./monthly-analysis.module.scss"

const { Text } = Typography

interface TotalSalesProps {
  storeId: string
}

export default function TotalSalesReport(props: TotalSalesProps) {
  const { storeId } = props
  const [monthlyTotalSales, setMonthlyTotalSales] = useState(new TotalSales())
  const [dailyTotalSales, setDailyTotalSales] = useState(new DailySale())

  useEffect(() => {
    SalesReport.queryTotalSales(
      new SalesQuery().toBuilder().storeId(storeId).build())
      .then(setMonthlyTotalSales)
  }, [storeId])

  useEffect(() => {
    SalesReport.queryTotalSales(
      new SalesQuery().toBuilder().storeId(storeId).build())
      .then(setDailyTotalSales)
  }, [storeId])


  return (
    <Card size="small">
      <Row justify="space-between">
        <Col>
          <Statistic title="月销售额" className={classes.MonthlyTotalStatistic} prefix="¥" value={monthlyTotalSales.amount}/>
        </Col>
        <Col>
          <Text type="secondary">
            <InfoCircleOutlined/>
          </Text>
        </Col>
      </Row>
      <Row className={classes.MonthlySalesTrends} align="bottom" gutter={[16, 0]}>
        <Col>
          <Trend>
            <Space>
              <Text>周同比</Text>
              <Text>10%</Text>
              <CaretDownOutlined style={{ fontSize: "12px", color: "#52c41a" }}/>
            </Space>
          </Trend>
        </Col>
        <Col>
          <Trend>
            <Space>
              <Text>日同比</Text>
              <Text>12%</Text>
              <CaretUpOutlined style={{ fontSize: "12px", color: "#f5222d" }}/>
            </Space>
          </Trend>
        </Col>
      </Row>
      <Divider/>
      <Space>
        <Text>日销售额</Text>
        <Text>￥{dailyTotalSales.amount}</Text>
      </Space>
    </Card>
  )
}
