import { Col, Row } from "antd"
import * as React from "react"
import classes from "./monthly-analysis.module.scss"
import MonthlyConversionRate from "./monthly-conversion-rate"
import MonthlyOrders from "./monthly-orders"
import MonthlyPages from "./monthly-pages"
import TotalSales from "./total-sales"

interface MonthlyAnalysisProps {
  storeId: string
}

export default function MonthlyAnalysis(props: MonthlyAnalysisProps) {
  const { storeId } = props
  return (
    <Row className={classes.MonthlyAnalysis} gutter={[16, 0]}>
      <Col span={6}>
        <TotalSales storeId={storeId}/>
      </Col>
      <Col span={6}>
        <MonthlyPages storeId={storeId}/>
      </Col>
      <Col span={6}>
        <MonthlyOrders storeId={storeId}/>
      </Col>
      <Col span={6}>
        <MonthlyConversionRate storeId={storeId}/>
      </Col>
    </Row>
  )
}
