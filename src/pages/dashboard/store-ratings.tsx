import { Radar } from "@ant-design/charts"
import { StoreRatingQuery, StoreRatingReport, TotalStoreRatings } from "@mallfoundry/report"
import { Card } from "antd"
import * as React from "react"
import { useEffect, useState } from "react"
import classes from "./product-ratings.module.scss"

const RATING_FIELD_NAME = "平均评分"

interface StoreRatingsRadarProps {
  storeId: string
}

function StoreRatingsRadar(props: StoreRatingsRadarProps) {
  const { storeId } = props
  const [totalStoreRatings, setTotalStoreRatings] = useState(new TotalStoreRatings())
  const asyncFetch = () => {
    StoreRatingReport
      .queryTotalStoreRatings(new StoreRatingQuery().toBuilder().storeId(storeId).build())
      .then(setTotalStoreRatings)
  }
  useEffect(asyncFetch, [storeId])

  const {
    productReviewRating,
    productPricingRating,
    customerServiceRating,
    productShippingRating,
  } = totalStoreRatings

  const data = [
    {
      type: "商品描述",
      [RATING_FIELD_NAME]: productPricingRating,
    },
    {
      type: "店铺好评",
      [RATING_FIELD_NAME]: productReviewRating,
    },
    {
      type: "服务态度",
      [RATING_FIELD_NAME]: customerServiceRating,
    },
    {
      type: "物流配送",
      [RATING_FIELD_NAME]: productShippingRating,
    },
  ]
  const config = {
    height: 238,
    // padding: [0, 0, 0, 0],
    autoFit: true,
    data,
    xField: "type",
    yField: [RATING_FIELD_NAME],
    xAxis: {
      line: null,
      tickLine: null,
      grid: {
        line: {
          style: {
            lineDash: null,
          },
        },
      },
    },
    yAxis: {
      line: null,
      tickLine: null,
      grid: {
        line: {
          type: "line",
          style: {
            lineDash: null,
          },
        },
        alternateColor: "rgba(0, 0, 0, 0.04)",
      },
    },
    point: {
      shape: "circle",
      size: 4,
    },
    area: {},
  }
  return <Radar {...config} />
}

interface StoreRatingsProps {
  storeId: string
}

export default function StoreRatings(props: StoreRatingsProps) {
  const { storeId } = props
  return (
    <Card title="客户满意度" size="small" className={classes.ProductRatings}>
      <StoreRatingsRadar storeId={storeId}/>
    </Card>
  )
}
