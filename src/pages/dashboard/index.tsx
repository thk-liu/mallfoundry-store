import PoweroffOutlined from "@ant-design/icons/PoweroffOutlined"
import RightOutlined from "@ant-design/icons/RightOutlined"
import UserOutlined from "@ant-design/icons/UserOutlined"
import { Store } from "@mallfoundry/store"
import { Avatar, Button, Card, Col, List, Popover, Row, Space, Statistic } from "antd"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useStore } from "../../hooks/store"
import { useCurrentUser } from "../../hooks/user"
import classes from "./index.module.scss"
import MonthlyAnalysis from "./monthly-analysis"
import OrderOverview from "./order-overview"
import ProductConversionRate from "./product-conversion-rate"
import ProductRatings from "./product-ratings"
import SalesAnalysis from "./sales-analysis"
import StoreRatings from "./store-ratings"
import { TotalProductQuantity } from "./total-quantity"

interface UserAvatarProps {
  store: Store
}

function UserAvatar(props: UserAvatarProps) {
  const { store } = props
  const user = useCurrentUser()

  const popoverContent = (
    <List itemLayout="horizontal">
      <List.Item>
        <Link to={`/stores`}>切换店铺</Link>
      </List.Item>
      <List.Item>
        <Link to="">
          访问店铺
          <span className={classes.listItemSecondaryText}>{store.name}<RightOutlined/></span>
        </Link>
      </List.Item>
      <List.Item>
        <Link to={`/users/${user.id}/personal`}>管理账号<span className={classes.listItemSecondaryText}><RightOutlined/></span>
        </Link>
      </List.Item>
      <List.Item>
        <Link to="/logout">退出登录<PoweroffOutlined/></Link>
      </List.Item>
    </List>
  )

  return (
    <Popover placement="bottomLeft"
             overlayClassName={classes.userPopover}
             content={popoverContent}
             trigger="click"
             align={{ offset: [20, 0] }}>
      <Space align="center" className={classes.userInfo}>
        <Avatar size="small" src={user.avatar} icon={<UserOutlined/>}/>
        {user.nickname}
        <RightOutlined/>
      </Space>
    </Popover>
  )
}

export default function Dashboard() {
  const { storeId = "" } = useParams<{ storeId?: string }>()
  const { store } = useStore(storeId)

  return (
    <div className={classes.root}>
      <Page.Header title={store.name} ghost={false} extra={<UserAvatar store={store}/>}/>
      <Page.Content className={classes.content}>
        <Row gutter={[16, 16]}>
          <Col span={24}>
            <MonthlyAnalysis storeId={storeId}/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={24}>
            <OrderOverview/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={24}>
            <SalesAnalysis storeId={storeId}/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <ProductRatings storeId={storeId}/>
          </Col>
          <Col span={6}>
            <StoreRatings storeId={storeId}/>
          </Col>
          <Col span={6}>
            <ProductConversionRate storeId={storeId}/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <TotalProductQuantity/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Card title="实时数据" size="small" extra={<Button type="link" size="small">数据中心</Button>}>
              <Row style={{ textAlign: "center" }} gutter={[0, 16]}>
                <Col span={8}>
                  <Statistic title="今日支付金额" value={0}/>
                </Col>
                <Col span={8}>
                  <Statistic title="今日访客数" value={0}/>
                </Col>
                <Col span={8}>
                  <Statistic title="今日订单数" value={0}/>
                </Col>
              </Row>
              <Row style={{ textAlign: "center" }}>
                <Col span={8}>
                  <Statistic title="今日支付买家数" value={0}/>
                </Col>
                <Col span={8}>
                  <Statistic title="今日浏览量" value={0}/>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Page.Content>
    </div>
  )
}
