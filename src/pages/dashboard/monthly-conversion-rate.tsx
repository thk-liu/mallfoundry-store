import { Bullet } from "@ant-design/charts"
import { BulletConfig } from "@ant-design/charts/lib/bullet"
import CaretDownOutlined from "@ant-design/icons/CaretDownOutlined"
import CaretUpOutlined from "@ant-design/icons/CaretUpOutlined"
import InfoCircleOutlined from "@ant-design/icons/InfoCircleOutlined"
import { ProductCountQuery, ProductCountReport, TotalProductCounts } from "@mallfoundry/report"
import { Card, Col, Divider, Row, Space, Statistic, Typography } from "antd"
import * as React from "react"
import { useEffect, useState } from "react"
import Trend from "../../components/trend"

const { Text } = Typography


interface ProductCountBulletProps {
  storeId: string
}

function ProductCountBullet(props: ProductCountBulletProps) {
  const { storeId } = props
  const [totalProductCounts, setTotalProductCounts] = useState(new TotalProductCounts())
  const asyncFetch = () => {
    ProductCountReport
      .queryTotalProductCounts(new ProductCountQuery().toBuilder().storeId(storeId).build())
      .then(setTotalProductCounts)
  }
  useEffect(asyncFetch, [storeId])
  const { viewCount = 0, addedCount = 0, placedCount = 0, paidCount = 0, completedCount = 0 } = totalProductCounts
  console.log(viewCount, addedCount, placedCount, paidCount, completedCount)
  const config: BulletConfig = {
    height: 60,
    autoFit: true,
    // padding: [12, 0, 12, 0],
    // rangeMax: 100,
    measureField: "measures",
    rangeField: "ranges",
    targetField: "target",
    data: [
      {
        title: "转化率",
        // measures: [viewCount, addedCount, placedCount, paidCount, completedCount],
        // targets: [viewCount + addedCount + placedCount],
        ranges: [100],
        measures: [30, 30, 10, 10],
        target: 85,
      },
    ],
    xAxis: {
      line: null,
      label: null,
    },
    yAxis: false,
    /* label: {
       measure: {
         offsetY: 1,
         position: "middle",
         style: {
           fill: "#fff",
         },
       },
       target: null,
     },*/
    /*   color: {
         range: "#F0F2F5",
         target: "#873BF4",
       },*/
    bulletStyle: {
      target: {
        lineWidth: 2,
        lineDash: [4, 16],
      },
    },
    size: {
      range: 14,
      measure: 14,
      target: 24,
    },
    tooltip: {
      showTitle: false,
      showMarkers: true,
      formatter: (datum: any) => {
        console.log(datum)
        return {
          name: "dskfajlk",
          value: datum.measures + "%",
        }
      },
    },
  }
  return <Bullet {...config} />
}

interface MonthlyConversionRateProps {
  storeId: string
}

export default function MonthlyConversionRate(props: MonthlyConversionRateProps) {
  const { storeId } = props
  return (
    <Card size="small">
      <Row justify="space-between">
        <Col>
          <Statistic title="商品转化" value={50} suffix="%"/>
        </Col>
        <Col>
          <Text type="secondary">
            <InfoCircleOutlined/>
          </Text>
        </Col>
      </Row>
      <ProductCountBullet storeId={storeId}/>
      <Divider/>
      <Row align="bottom" gutter={[16, 0]}>
        <Col>
          <Trend>
            <Space>
              <Text>周同比</Text>
              <Text>2%</Text>
              <CaretDownOutlined style={{ fontSize: "12px", color: "#52c41a" }}/>
            </Space>
          </Trend>
        </Col>
        <Col>
          <Trend>
            <Space>
              <Text>周同比</Text>
              <Text>5%</Text>
              <CaretUpOutlined style={{ fontSize: "12px", color: "#f5222d" }}/>
            </Space>
          </Trend>
        </Col>
      </Row>
    </Card>
  )
}
