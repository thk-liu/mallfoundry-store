import { DisputeQuery, DisputeService, DisputeStatus, OrderQuery, OrderService, OrderStatus } from "@mallfoundry/order"
import { Card, Col, Row, Statistic } from "antd"
import * as React from "react"
import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"


const AWAITING_PAYMENT_STATUSES = [OrderStatus.Pending, OrderStatus.AwaitingPayment]
const AWAITING_PAYMENT_STATUS_VALUES = AWAITING_PAYMENT_STATUSES.join(",")
const AWAITING_SHIPMENT_STATUSES = [OrderStatus.AwaitingFulfillment, OrderStatus.AwaitingShipment, OrderStatus.PartiallyShipped]
const AWAITING_SHIPMENT_STATUS_VALUES = AWAITING_SHIPMENT_STATUSES.join(",")
const SHIPPED_STATUSES = [OrderStatus.Shipped, OrderStatus.AwaitingPickup]
const SHIPPED_STATUS_VALUES = SHIPPED_STATUSES.join(",")
const DISPUTED_STATUSES = [DisputeStatus.Applying, DisputeStatus.Reapplying]

function useOrderCount(storeId: string, statuses: any[]) {
  const [count, setCount] = useState(0)
  useEffect(() => {
    OrderService.countOrders(new OrderQuery().toBuilder().storeId(storeId)
      .statuses(statuses).build())
      .then(setCount)
  }, [storeId, statuses])
  return count
}

function useDisputeCount(storeId: string, statuses: any[]) {
  const [count, setCount] = useState(0)
  useEffect(() => {
    DisputeService.countOrderDisputes(new DisputeQuery().toBuilder()
      .storeId(storeId).statuses(statuses).build())
      .then(setCount)
  }, [storeId, statuses])
  return count
}


interface RouterParams {
  storeId: string
}

export default function OrderOverview() {
  const { storeId } = useParams<RouterParams>()
  const awaitingPaymentOrderCount = useOrderCount(storeId, AWAITING_PAYMENT_STATUSES)
  const awaitingShipCount = useOrderCount(storeId, AWAITING_SHIPMENT_STATUSES)
  const awaitingPickupCount = useOrderCount(storeId, SHIPPED_STATUSES)
  const awaitingDisputeCount = useDisputeCount(storeId, DISPUTED_STATUSES)
  return (
    <Card>
      <Row style={{ textAlign: "center" }}>
        <Col span={4}>
          <Link to={{ pathname: `orders`, search: `statuses=${AWAITING_PAYMENT_STATUS_VALUES}` }}>
            <Statistic title="待付款" value={awaitingPaymentOrderCount}/>
          </Link>
        </Col>
        <Col span={4}>
          <Link to={{ pathname: `orders`, search: `statuses=${AWAITING_SHIPMENT_STATUS_VALUES}` }}>
            <Statistic title="待发货" value={awaitingShipCount}/>
          </Link>
        </Col>
        <Col span={4}>
          <Link to={{ pathname: `orders`, search: `statuses=${SHIPPED_STATUS_VALUES}` }}>
            <Statistic title="待签收" value={awaitingPickupCount}/>
          </Link>
        </Col>
        <Col span={4}>
          <Link to={{ pathname: `orders/disputes` }}>
            <Statistic title="退款/售后" value={awaitingDisputeCount}/>
          </Link>
        </Col>
        <Col span={4}>
          <Statistic title="物流异常" value={0}/>
        </Col>
        <Col span={4}>
          <Statistic title="违规" value={0}/>
        </Col>
      </Row>
    </Card>
  )
}
