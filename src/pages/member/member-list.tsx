import { Gender } from "@mallfoundry/keystone/identity"
import { Member } from "@mallfoundry/store/member"
import { Button, Card, Col, Divider, Row, Table } from "antd"
import { ColumnProps } from "antd/lib/table"
import * as _ from "lodash"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useMembers } from "../../hooks/member"

export default function MemberList() {
  const { storeId } = useParams<{ storeId: string }>()
  const columns: ColumnProps<Member>[] = [
    {
      title: "客户",
      dataIndex: "nickname",
      key: "nickname",
    },
    {
      title: "手机号",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "性别",
      dataIndex: "gender",
      key: "gender",
      render: (gender: Gender) => (
        <>
          {Gender.Unknown === gender && "保密"}
          {Gender.Male === gender && "男"}
          {Gender.Female === gender && "女"}
        </>
      ),
    },
    {
      title: "生日",
      dataIndex: "birthdate",
      key: "birthdate",
    },
    {
      title: "标签",
      dataIndex: "tags",
      key: "tags",
      align: "center",
      render: (tags: string[]) => _.isEmpty(tags) ? "-" : _.join(tags, ","),
    },
    {
      title: "加入时间",
      dataIndex: "joinedTime",
      key: "joinedTime",
      width: "160px",
    },
    {
      title: "操作",
      key: "id",
      dataIndex: "id",
      align: "right",
      width: "140px",
      render: (id: string) => (
        <>
          <Link to={`/stores/${storeId}/members/${id}`}>查看</Link>
          <Divider type="vertical"/>
          <Link to={`/stores/${storeId}/members/${id}/edit`}>编辑</Link>
        </>
      ),
    },
  ]
  const { loading, elements: members } = useMembers({ storeId })
  return (
    <div>
      <Page.Header title="会员管理" ghost={false}/>
      <Page.Content>
        <Card>
          <Row gutter={[16, 16]}>
            <Col><Button type="primary">
              <Link to={`/stores/${storeId}/members/new`}>添加会员</Link></Button></Col>
          </Row>
          <Row>
            <Col span={24}>
              <Table rowKey="id" columns={columns} dataSource={members} loading={loading}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
