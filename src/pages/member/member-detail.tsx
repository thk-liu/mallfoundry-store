import { Gender } from "@mallfoundry/keystone/identity"
import { Card, Col, Form, Row } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useParams } from "react-router-dom"
import Page from "../../components/page"
import { useMember } from "../../hooks/member"

export default function MemberDetail() {
  const { storeId, memberId } = useParams<{ storeId: string, memberId: string }>()
  const { loading, member } = useMember(storeId, memberId)
  return (
    <div>
      <Page.Header title="会员详情" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Row>
            <Col span={24}>
              <Form>
                <Form.Item label="会员账号" name="id">
                  <span className="ant-form-text">{member.id}</span>
                </Form.Item>
                <Form.Item label="姓名" name="nickname">
                  <span className="ant-form-text">{member.nickname}</span>
                </Form.Item>
                <Form.Item label="性别" name="gender">
                  <span className="ant-form-text">
                    {Gender.Unknown === member.gender && "保密"}
                    {Gender.Male === member.gender && "男"}
                    {Gender.Female === member.gender && "女"}
                  </span>
                </Form.Item>
                <Form.Item label="生日" name="birthdate">
                  <span className="ant-form-text">{member.birthdate}</span>
                </Form.Item>
                <Form.Item label="备注" name="notes">
                  <span className="ant-form-text">{_.isEmpty(member.notes) ? "-" : member.notes}</span>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
