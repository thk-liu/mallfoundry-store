import { getResponseMessage } from "@mallfoundry/client/message"
import { Member, MemberService } from "@mallfoundry/store/member"
import { Card, Col, message as antMessage, Row } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../components/page"
import MemberForm from "./member-form"

export default function MemberNew() {
  const { storeId } = useParams<{ storeId: string }>()
  const history = useHistory()
  const [loading, setLoading] = useState(false)

  function finish(values: Member) {
    setLoading(loading)
    MemberService.addMember(values)
      .then(() => {
        antMessage.success("添加成功")
      })
      .then(() => history.goBack())
      .catch(error => antMessage.error(getResponseMessage(error)))
      .finally(() => setLoading(false))
  }

  return (
    <div>
      <Page.Header title="添加会员" ghost={false}/>
      <Page.Content>
        <Card>
          <Row>
            <Col span={24}>
              <MemberForm
                loading={loading}
                member={_.assign(new Member(), { storeId })}
                onFinish={finish}
                onCancel={() => history.push(`/stores/${storeId}/members`)}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
