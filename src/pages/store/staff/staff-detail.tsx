import { Card, Form } from "antd"
import * as React from "react"
import { useParams } from "react-router-dom"
import Page from "../../../components/page"
import { useStaff } from "../../../hooks/staff"

export default function StaffList() {
  const { storeId, staffId } = useParams<{ storeId: string, staffId: string }>()
  const { loading, staff } = useStaff(storeId, staffId)
  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 4 },
  }
  return (
    <div>
      <Page.Header title="员工详情" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Form {...layout}>
            <Form.Item label="员工账号">
              <span className="ant-form-text">{staff.id}</span>
            </Form.Item>
            <Form.Item label="员工姓名">
              <span className="ant-form-text">{staff.name}</span>
            </Form.Item>
            <Form.Item label="员工编号">
              <span className="ant-form-text">{staff.number}</span>
            </Form.Item>
            <Form.Item label="联系方式" name="phone">
              <span className="ant-form-text">{staff.phone}</span>
            </Form.Item>
          </Form>
        </Card>
      </Page.Content>
    </div>
  )
}
