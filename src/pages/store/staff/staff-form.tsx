import { Role } from "@mallfoundry/store/role"
import { Staff, StaffType } from "@mallfoundry/store/staff"
import { Button, Form, Input, Select, Space } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useEffect } from "react"
import { useParams } from "react-router-dom"
import { useRoles } from "../../../hooks/role"

class StaffObject {
  public id: string = ""
  public name: string = ""
  public number: string = ""
  public storeId: string = ""
  public phone: string = ""
  public countryCode: string = ""
  public roles: string[] = []
}

interface StaffFormProps {
  loading: boolean
  staff: Staff
  onFinish?: (staff: Staff) => void
  onCancel?: () => void
  idReadonly?: boolean
}

export default function StaffForm(props: StaffFormProps) {
  const { storeId } = useParams<{ storeId: string }>()
  const { loading, staff, onFinish: finish, onCancel, idReadonly } = props
  const { elements: roles } = useRoles({ storeId })

  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 4 },
  }
  const tailLayout = {
    wrapperCol: { offset: 2, span: 16 },
  }

  const onFinish = (values: any) => {
    if (_.isFunction(finish)) {
      const roleIds = _.isString(values.roles) ? [values.roles] : values.roles
      finish(_.assign(new Staff(), staff, values, {
        countryCode: "86",
        roles: _.map(roleIds, id => _.assign(new Role(), { id })),
      }))
    }
  }

  const [form] = Form.useForm()
  useEffect(() => {
    if (_.isEmpty(staff.countryCode)) {
      staff.countryCode = "86"
    }
    const roleIds = _.map(staff.roles, role => role.id)
    form.setFieldsValue(_.assign(new StaffObject(), staff, {
      countryCode: staff.countryCode,
      roles: roleIds,
    }))
  }, [form, staff])
  return (
    <Form {...layout} form={form}
          onFinish={onFinish}>
      <Form.Item label="员工账号" name="id" rules={[{ required: true, message: "请输入员工账号" }]}>
        {idReadonly ? <span className="ant-form-text">{staff.id}</span> : <Input/>}
      </Form.Item>
      <Form.Item label="员工姓名" name="name"
                 rules={[{ required: true, message: "请输入员工姓名" }]}>
        <Input/>
      </Form.Item>
      <Form.Item label="员工编号" name="number">
        <Input/>
      </Form.Item>
      <Form.Item label="联系方式" name="phone" rules={[{ required: true, message: "手机号不能为空" }]}>
        <Input/>
      </Form.Item>
      <Form.Item label="角色" name="roles">
        <Select mode="multiple" disabled={staff.type === StaffType.Owner}>
          {
            _.map(roles, role => (
              <Select.Option
                key={role.id}
                value={role.id as string}
                children={role.name}/>
            ))
          }
        </Select>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space>
          <Button type="primary" htmlType="submit" loading={loading}>保存</Button>
          <Button htmlType="button" onClick={onCancel}>取消</Button>
        </Space>
      </Form.Item>
    </Form>
  )
}
