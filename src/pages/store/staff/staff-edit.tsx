import { Staff, StaffService } from "@mallfoundry/store/staff"
import { Card, Col, message, Row } from "antd"
import * as React from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../../components/page"
import { useLoading } from "../../../hooks/loading"
import { useStaff } from "../../../hooks/staff"
import StaffForm from "./staff-form"

export default function StaffEdit() {
  const { storeId, staffId } = useParams<{ storeId: string, staffId: string }>()
  const history = useHistory()
  const { loading: getLoading, staff } = useStaff(storeId, staffId)
  const [loading, setLoading] = useLoading([getLoading])

  function finish(values: Staff) {
    setLoading(true)
    StaffService.updateStaff(values)
      .then(() => {
        message.success("保存成功")
      })
      .then(() => history.goBack())
      .finally(() => setLoading(false))
  }

  return (
    <div>
      <Page.Header title="编辑员工" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Row>
            <Col span={24}>
              <StaffForm
                loading={loading}
                staff={staff}
                idReadonly
                onFinish={finish} onCancel={() => history.push(`/stores/${storeId}/staffs`)}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
