import { Card, Col, Descriptions, Row } from "antd"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useStore } from "../../hooks/store"
import classes from "./store-detail.module.scss"

export default function StoreDetail() {
  const { storeId = "" } = useParams<{ storeId: string }>()
  const { store } = useStore(storeId)

  return (
    <div className={classes.storeDetail}>
      <Page.Header title="店铺信息" ghost={false}/>
      <Page.Content>
        <Card className={classes.storeInfo} title="店铺信息" size="small"
              extra={<Link to={`/stores/${storeId}/edit`}>编辑</Link>}>
          <Row gutter={[24, 0]}>
            <Col span={2}>
              <img className={classes.storeLogo} src={store.logo} alt=""/>
            </Col>
            <Col span={12}>
              <Descriptions title={store.name} column={2}>
                <Descriptions.Item label="店铺编号">{store.id}</Descriptions.Item>
                <Descriptions.Item label="创建时间">{store.createdTime}</Descriptions.Item>
                <Descriptions.Item label="店铺简介">{store.description}</Descriptions.Item>
              </Descriptions>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )

}
