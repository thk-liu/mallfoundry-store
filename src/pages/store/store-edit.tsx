import { Store, StoreService } from "@mallfoundry/store"
import { Button, Card, Form, Input } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../components/page"
import { useStore } from "../../hooks/store"
import { ImageBlobsModal } from "./blobs/image-blobs"
import classes from "./store-edit.module.scss"

export default function StoreEdit() {
  const [form] = Form.useForm()
  const { storeId = "" } = useParams<{ storeId: string }>()
  const history = useHistory()
  const [loading, setLoading] = useState(false)
  const [popVisible, setPopVisible] = useState(false)
  const { store, setStore } = useStore(storeId)

  useEffect(() => {
    form.setFieldsValue(store)
  }, [form, store])

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
  }
  const tailLayout = {
    wrapperCol: { offset: 4, span: 8 },
  }
  const onFinish = (values: any) => {
    setLoading(prevLoading => prevLoading && true)
    const newStore = _.assign(new Store(), store, values)
    StoreService.updateStore(newStore)
      .then(() => history.push(`/stores/${storeId}/detail`))
      .finally(() => setLoading(false))
  }

  return (
    <div className={classes.StoreEdit}>
      <ImageBlobsModal selectable
                       multiple={false}
                       visible={popVisible}
                       onSelect={blobs => {
                         const blob = _.chain(blobs).first().value()
                         setStore(_.assign(new Store(), store, { logo: blob.url }))
                         setPopVisible(false)
                       }}
                       onCancel={() => setPopVisible(false)}/>
      <Page.Header title="店铺信息" ghost={false}/>
      <Page.Content>
        <Card size="small">
          <Form
            {...layout}
            form={form}
            onFinish={onFinish}>
            <Form.Item label="店铺LOGO" required>
              <img className={classes.StoreLogo} src={store.logo} alt=""/>
              <Button size="small" type="link"
                      onClick={() => setPopVisible(!popVisible)}>上传LOGO</Button>
              <Form.Item name="logo"
                         rules={[{ required: true, message: "请上传店铺LOGO!" }]}
                         noStyle>
                <Input hidden/>
              </Form.Item>
            </Form.Item>
            <Form.Item
              label="店铺名称"
              name="name"
              rules={[{ required: true, message: "请填写店铺名称!" }]}
              extra="店铺名称应尽量简洁易记，建议15字以内">
              <Input/>
            </Form.Item>
            {/*            <Form.Item
              label="主营类目"
              name="category">
              <span className="ant-form-text">女装</span>
            </Form.Item>*/}
            <Form.Item
              label="创建日期"
              name="category">
              <span className="ant-form-text">{store.createdTime}</span>
            </Form.Item>
            <Form.Item
              label="店铺简介"
              name="description">
              <Input.TextArea
                autoSize={{ minRows: 4, maxRows: 6 }}/>
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button loading={loading} type="primary" htmlType="submit">保存</Button>
            </Form.Item>
          </Form>
        </Card>
      </Page.Content>
    </div>
  )
}
