import { Role, RoleService } from "@mallfoundry/store/role"
import { Card, Col, message, Row } from "antd"
import * as React from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../../components/page"
import { useLoading } from "../../../hooks/loading"
import { useRole } from "../../../hooks/role"
import RoleForm from "./role-form"

export default function RoleEdit() {
  const { storeId, roleId } = useParams<{ storeId: string, roleId: string }>()
  const history = useHistory()
  const { loading: roleLoading, role } = useRole(storeId, roleId)
  const [loading, setLoading] = useLoading(roleLoading)

  function finish(values: Role) {
    setLoading(true)
    RoleService.updateRole(values)
      .then(() => {
        message.success("保存成功")
      })
      .then(() => history.goBack())
      .finally(() => setLoading(false))
  }

  return (
    <div>
      <Page.Header title="编辑角色" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Row>
            <Col span={24}>
              <RoleForm loading={loading} role={role}
                        onFinish={finish}
                        onCancel={() => history.push(`/stores/${storeId}/roles`)}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
