import { getResponseMessage } from "@mallfoundry/client/message"
import { Role, RoleService } from "@mallfoundry/store/role"
import { Card, Col, message as antMessage, Row } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../../components/page"
import RoleForm from "./role-form"

export default function RoleNew() {
  const { storeId } = useParams<{ storeId: string }>()
  const history = useHistory()
  const [loading, setLoading] = useState(false)

  function finish(values: Role) {
    setLoading(true)
    RoleService.addRole(values)
      .then(() => {
        antMessage.success("添加成功")
      })
      .then(() => history.goBack())
      .catch(error => antMessage.error(getResponseMessage(error)))
      .finally(() => setLoading(false))
  }

  return (
    <div>
      <Page.Header title="添加角色" ghost={false}/>
      <Page.Content>
        <Card>
          <Row>
            <Col span={24}>
              <RoleForm loading={loading}
                        role={_.assign(new Role(), { storeId })}
                        onFinish={finish}
                        onCancel={() => history.push(`/stores/${storeId}/roles`)}/>
            </Col>
          </Row>
        </Card>
      </Page.Content>
    </div>
  )
}
