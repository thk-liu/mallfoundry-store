import { Card, Form } from "antd"
import * as React from "react"
import { useParams } from "react-router-dom"
import Page from "../../../components/page"
import { useRole } from "../../../hooks/role"
import RoleAuthorities from "./role-authorities"

const layout = {
  labelCol: { span: 2 },
  wrapperCol: { span: 4 },
}

export default function RoleDetail() {
  const { storeId, roleId } = useParams<{ storeId: string, roleId: string }>()
  const { loading, role } = useRole(storeId, roleId)
  return (
    <div>
      <Page.Header title="角色详情" ghost={false}/>
      <Page.Content loading={loading}>
        <Card>
          <Form {...layout}>
            <Form.Item label="角色名称">
              <span className="ant-form-text">{role.name}</span>
            </Form.Item>
            <Form.Item label="角色描述">
              <span className="ant-form-text">{role.description}</span>
            </Form.Item>
            <Form.Item label="权限">
              <RoleAuthorities authorities={role.authorities} readonly/>
            </Form.Item>
          </Form>
        </Card>
      </Page.Content>
    </div>
  )
}
