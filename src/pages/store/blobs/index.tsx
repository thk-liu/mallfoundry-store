import { Card, PageHeader, Tabs } from "antd"
import * as React from "react"
import ImageBlobs from "./image-blobs"
import classes from "./index.module.scss"

const { TabPane } = Tabs

export default function Blobs() {
  return (
    <div className={classes.blobs}>
      <PageHeader title="素材空间" ghost={false}/>
      <div className={classes.blobsContent}>
        <Card size="small">
          <Tabs defaultActiveKey="1">
            <TabPane tab="图片" key="1">
              <ImageBlobs/>
            </TabPane>
          </Tabs>
        </Card>
      </div>
    </div>
  )
}
