import { Coupon, CouponService } from "@mallfoundry/marketing/coupon"
import { Card } from "antd"
import * as _ from "lodash"
import * as React from "react"
import { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Page from "../../components/page"
import { messageError } from "../../utils/reason"
import classes from "./coupon-copy.module.scss"
import CouponForm from "./coupon-form"


export default function CouponCopy() {
  const { storeId = "", couponId = "" } = useParams<{ storeId: string, couponId: string }>()
  const history = useHistory()
  const [coupon, setCoupon] = useState<Coupon>(_.assign(new Coupon(), { storeId }))
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    CouponService
      .getCoupon(couponId)
      .then(setCoupon)
      .finally(() => setLoading(false))
  }, [couponId])

  function onSave(aCoupon: Coupon) {
    setLoading(true)
    CouponService
      .addCoupon(aCoupon)
      .then(() => setLoading(false))
      .then(() => history.push(`/stores/${storeId}/coupons`))
      .catch((reason: string) => {
        messageError(reason)
        setLoading(false)
      })
  }

  return (<div className={classes.CouponCopy}>
    <Page.Header title="添加优惠券" ghost={false}/>
    <Page.Content loading={loading}>
      <Card>
        <CouponForm loading={false} coupon={coupon}
                    onFinish={onSave}
                    onCancel={() => history.push(`/stores/${storeId}/coupons`)}/>
      </Card>
    </Page.Content>
  </div>)
}
