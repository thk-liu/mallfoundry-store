import { Coupon, CouponType } from "@mallfoundry/marketing/coupon"
import { Typography } from "antd"
import * as _ from "lodash"
import * as React from "react"
import classes from "./coupon-preview.module.scss"

const { Paragraph } = Typography

interface CouponPreviewProps {
  coupon: Coupon
}

export default function CouponPreview(props: CouponPreviewProps) {
  const { coupon } = props
  const {
    name, type, discountAmount, discountPercent,
    discountMinAmount, discountMaxAmount,
    minAmount, startTime, endTime, description,
  } = coupon
  const validatePeriod = _.isUndefined(startTime) || _.isUndefined(endTime) ? <>无限期</> : <>{startTime}-{endTime}</>
  return (
    <div className={classes.CouponPreview}>
      <div className={classes.CouponPreviewContent}>
        <div className={classes.CouponHeader}>
          <div className={classes.CouponTitle}>优惠券</div>
        </div>
        <div className={classes.CouponType}>(微商城优惠券)</div>
        <div className={classes.CouponContent}>
          <div className={classes.CouponName}>{name}</div>
          <div className={classes.CouponValue}>
            {type === CouponType.FixedDiscount && discountAmount && <>减免{discountAmount}元优惠券</>}
            {type === CouponType.PercentageDiscount && discountPercent && <>{discountPercent}折优惠券</>}
            {type === CouponType.RandomDiscount && discountMinAmount && discountMaxAmount && <>随机优惠{discountMinAmount}至{discountMaxAmount}元</>}
          </div>
          <Paragraph className={classes.CouponMinAmount}>
            {_.isUndefined(minAmount) ? <>满任意金额可用</> : minAmount > 0 && <>满{minAmount}元可用</>}
          </Paragraph>
          <div className={classes.CouponValidatePeriod}>
            有效日期：{validatePeriod}
          </div>
          <div className={classes.CouponContentTail}/>
        </div>
        <div className={classes.CouponDescriptionTitle}>使用说明</div>
        <div className={classes.CouponDescriptionDetail}>
          {name && <Paragraph>{name}</Paragraph>}
          <Paragraph>活动时间：{validatePeriod}</Paragraph>
          <Paragraph>{description}</Paragraph>
        </div>
      </div>
    </div>
  )
}
