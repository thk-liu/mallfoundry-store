import AppstoreAddOutlined from "@ant-design/icons/AppstoreAddOutlined"
import DashboardOutlined from "@ant-design/icons/DashboardOutlined"
import GiftOutlined from "@ant-design/icons/GiftOutlined"
import PayCircleOutlined from "@ant-design/icons/PayCircleOutlined"
import PoweroffOutlined from "@ant-design/icons/PoweroffOutlined"
import ProfileOutlined from "@ant-design/icons/ProfileOutlined"
import RightOutlined from "@ant-design/icons/RightOutlined"
import SettingOutlined from "@ant-design/icons/SettingOutlined"
import ShopOutlined from "@ant-design/icons/ShopOutlined"
import TagOutlined from "@ant-design/icons/TagOutlined"
import UserOutlined from "@ant-design/icons/UserOutlined"
import { SubjectHolder } from "@mallfoundry/keystone/security"
import { Avatar, ConfigProvider, Layout, List, Menu, Popover } from "antd"
import zhCN from "antd/es/locale/zh_CN"
import classNames from "classnames"
import "dayjs/locale/zh-cn"
import * as _ from "lodash"
import * as React from "react"
import { lazy, Suspense, useState } from "react"
import { BrowserRouter, Link, Redirect, Route, Switch, useHistory, useParams } from "react-router-dom"
import Login from "./account/login"
import "./App.css"
import classes from "./App.module.scss"
import PrivateRoute from "./components/access"
import { useStore } from "./hooks/store"

interface RouterParams {
  storeId: string
}

interface StoreHeaderProps {
  collapsed: boolean
}

function StoreHeader({ collapsed }: StoreHeaderProps) {
  const history = useHistory()
  const { storeId = "" } = useParams<RouterParams>()
  const { store } = useStore(storeId)

  const popoverContent = (
    <List itemLayout="horizontal">
      <List.Item onClick={() => history.push(`/stores/${storeId}/dashboard`)}>店铺概况</List.Item>
      <List.Item onClick={() => history.push(`/stores`)}>切换店铺</List.Item>
      <List.Item>
        访问店铺
        <span className={classes.listItemSecondaryText}>{store.name}<RightOutlined/></span>
      </List.Item>
      <List.Item>管理账号
        <span className={classes.listItemSecondaryText}><RightOutlined/></span>
      </List.Item>
      <List.Item onClick={() => {
        SubjectHolder.logout().then(() => history.replace("/"))
      }}>退出登录<PoweroffOutlined/></List.Item>
    </List>
  )

  return (
    <Popover placement="bottomLeft"
             overlayClassName={classes.StoreHeaderMenus}
             content={popoverContent}
             trigger="click"
             align={{ offset: [20, 0] }}>
      <div className={classNames(classes.StoreHeader, {
        [classes.storeHeaderCollapsed]: collapsed,
      })}>
        <Avatar src={store.logo} icon={<ShopOutlined/>}/>
        {!collapsed && <span>{store.name}</span>}
      </div>
    </Popover>
  )
}

interface NavMenusProps {
  collapsed: boolean
}

function NavMenus({ collapsed }: NavMenusProps) {
  const history = useHistory()
  const { storeId } = useParams<RouterParams>()
  return (
    <div className={classes.navMenus}>
      <div className={classes.mainMenus}>
        <Menu
          theme="dark"
          mode="inline">
          <Menu.Item key="dashboard"
                     onClick={() => history.push(`/stores/${storeId}/dashboard`)}>
            <DashboardOutlined/>
            <span>概况</span>
          </Menu.Item>
          <Menu.SubMenu
            key="store"
            title={<><ShopOutlined/><span>店铺</span></>}>
            <Menu.Item key="attachments">
              <Link to={`/stores/${storeId}/blobs`}>素材空间</Link>
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.SubMenu
            title={<><TagOutlined/><span>商品</span></>}>
            <Menu.Item key="products">
              <Link to={`/stores/${storeId}/products`}>商品管理</Link>
            </Menu.Item>
            <Menu.Item key="collections">
              <Link to={`/stores/${storeId}/collections`}>商品集合</Link></Menu.Item>
          </Menu.SubMenu>
          <Menu.SubMenu
            title={<><UserOutlined/><span>会员</span></>}>
            <Menu.Item key="members">
              <Link to={`/stores/${storeId}/members`}>会员管理</Link>
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.SubMenu
            title={<><GiftOutlined/><span>营销</span></>}>
            <Menu.Item key="coupons">
              <Link to={`/stores/${storeId}/coupons`}>优惠券</Link>
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.SubMenu
            title={<><ProfileOutlined/><span>订单</span></>}>
            <Menu.Item key="orders">
              <Link to={`/stores/${storeId}/orders`}>订单管理</Link>
            </Menu.Item>
            <Menu.Item key="orders/disputes">
              <Link to={`/stores/${storeId}/orders/disputes`}>售后维权</Link>
            </Menu.Item>
            {/*<Menu.Item key="shipping-rates">
              <Link to={`/stores/${storeId}/shipping-rates`}>物流发货</Link>
            </Menu.Item>*/}
          </Menu.SubMenu>
          <Menu.SubMenu
            title={<><PayCircleOutlined/><span>资产</span></>}>
            <Menu.Item key="balances">
              <Link to={`/stores/${storeId}/balances`}>余额</Link>
            </Menu.Item>
            <Menu.Item key="WithdrawalApply">
              <Link to={`/stores/${storeId}/withdrawals/apply`}>提现</Link>
            </Menu.Item>
            <Menu.Item key="TopupNew">
              <Link to={`/stores/${storeId}/topups/new`}>充值</Link>
            </Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </div>
      <Menu
        theme="dark"
        mode="vertical"
        selectable={false}
        className={classes.fixedMenus}>
        <Menu.Item key="addApp">
          <AppstoreAddOutlined/>
          <span>应用</span>
        </Menu.Item>
        <Menu.SubMenu title={<><SettingOutlined/><span>设置</span></>}>
          <Menu.Item>
            <Link to={`/stores/${storeId}/detail`}>店铺信息</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`/stores/${storeId}/staffs`}>员工管理</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`/stores/${storeId}/roles`}>角色管理</Link>
          </Menu.Item>
        </Menu.SubMenu>
      </Menu>
    </div>
  )
}

const routes = [
  {
    exact: true,
    path: "/stores/:storeId/dashboard",
    component: lazy(() => import("./pages/dashboard")),
  },
  {
    exact: true,
    path: "/stores/:storeId/collections",
    component: lazy(() => import("./pages/collection/collection-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/blobs",
    component: lazy(() => import("./pages/store/blobs")),
  },
  {
    exact: true,
    path: "/stores/:storeId/products/reviews",
    component: lazy(() => import("./pages/product/review/products-review-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/products/new",
    component: lazy(() => import("./pages/product/product-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/products/:productId",
    component: lazy(() => import("./pages/product/product-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/products",
    component: lazy(() => import("./pages/product/product-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/orders/:orderId/refunds/:refundId",
    component: lazy(() => import("./pages/order/order-refund-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/orders/disputes",
    component: lazy(() => import("./pages/order/orders-dispute-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/orders",
    component: lazy(() => import("./pages/order/order-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/orders/:orderId",
    component: lazy(() => import("./pages/order/order-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/detail",
    component: lazy(() => import("./pages/store/store-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/edit",
    component: lazy(() => import("./pages/store/store-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/shipping-rates",
    component: lazy(() => import("./pages/shipping/shipping-rate-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/shipping-rates/new",
    component: lazy(() => import("./pages/shipping/shipping-rate-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/staffs/new",
    component: lazy(() => import("./pages/store/staff/staff-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/staffs/:staffId/edit",
    component: lazy(() => import("./pages/store/staff/staff-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/staffs/:staffId",
    component: lazy(() => import("./pages/store/staff/staff-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/staffs",
    component: lazy(() => import("./pages/store/staff/staff-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/roles",
    component: lazy(() => import("./pages/store/role/role-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/roles/new",
    component: lazy(() => import("./pages/store/role/role-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/roles/:roleId/edit",
    component: lazy(() => import("./pages/store/role/role-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/roles/:roleId",
    component: lazy(() => import("./pages/store/role/role-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/members",
    component: lazy(() => import("./pages/member/member-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/members/new",
    component: lazy(() => import("./pages/member/member-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/members/:memberId/edit",
    component: lazy(() => import("./pages/member/member-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/members/:memberId",
    component: lazy(() => import("./pages/member/member-detail")),
  },
  {
    exact: true,
    path: "/stores/:storeId/coupons",
    component: lazy(() => import("./pages/marketing/coupon-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/coupons/new",
    component: lazy(() => import("./pages/marketing/coupon-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/coupons/:couponId/edit",
    component: lazy(() => import("./pages/marketing/coupon-edit")),
  },
  {
    exact: true,
    path: "/stores/:storeId/coupons/:couponId/copy",
    component: lazy(() => import("./pages/marketing/coupon-copy")),
  },
  {
    exact: true,
    path: "/stores/:storeId/balances",
    component: lazy(() => import("./pages/finance/account/balance")),
  },
  {
    exact: true,
    path: "/stores/:storeId/bank-cards",
    component: lazy(() => import("./pages/bank/bank-card-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/withdrawals/apply",
    component: lazy(() => import("./pages/finance/withdrawal-apply")),
  },
  {
    exact: true,
    path: "/stores/:storeId/withdrawals",
    component: lazy(() => import("./pages/finance/withdrawal-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/withdrawals/:withdrawalId/progress",
    component: lazy(() => import("./pages/finance/withdrawal-progress")),
  },
  {
    exact: true,
    path: "/stores/:storeId/topups/new",
    component: lazy(() => import("./pages/finance/topup-new")),
  },
  {
    exact: true,
    path: "/stores/:storeId/topups",
    component: lazy(() => import("./pages/finance/topup-list")),
  },
  {
    exact: true,
    path: "/stores/:storeId/transactions",
    component: lazy(() => import("./pages/finance/transaction-list")),
  },
]

function BaseLayout() {
  const { Content, Sider } = Layout
  const [collapsed, setCollapsed] = useState(true)

  return (
    <Layout className="site-layout">
      <Sider width={220} collapsible collapsed={collapsed} onCollapse={setCollapsed}>
        <StoreHeader collapsed={collapsed}/>
        <NavMenus collapsed={collapsed}/>
      </Sider>
      <Content>
        <Suspense fallback={<div>loading</div>}>
          <Switch>
            {_.map(routes, route => <PrivateRoute key={route.path} exact={route.exact} path={route.path} component={route.component}/>)}
          </Switch>
        </Suspense>
      </Content>
    </Layout>
  )
}

function App() {
  return (
    <ConfigProvider locale={zhCN}>
      <BrowserRouter>
        <Suspense fallback={<div>loading</div>}>
          <Switch>
            <Route exact path="/Login" component={Login}/>
            <Route exact path="/signup" component={lazy(() => import("./account/signup"))}/>
            <Route exact path="/retake" component={lazy(() => import("./account/retake"))}/>
            <PrivateRoute exact path="/logout" component={lazy(() => import("./account/logout"))}/>
            <Redirect exact path="/" to="/stores"/>
            <PrivateRoute exact path="/stores" component={lazy(() => import("./pages/store/store-list"))}/>
            <PrivateRoute exact path="/stores/create" component={lazy(() => import("./pages/store/store-create"))}/>
            <PrivateRoute exact path="/users/:userId/personal" component={lazy(() => import("./pages/user/personal"))}/>
            <PrivateRoute path="/stores/:storeId" component={BaseLayout}/>
          </Switch>
        </Suspense>
      </BrowserRouter>
    </ConfigProvider>
  )
}

export default App
