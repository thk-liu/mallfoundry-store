import * as React from "react"
import classes from "./index.module.scss"

interface BlockHeaderProps {
  title?: React.ReactNode
  children?: React.ReactNode
  justify?: "space-between"
}

export default function BlockHeader(props: BlockHeaderProps) {
  const { title, children, justify } = props
  return (
    <div className={classes.BlockHeader} style={{
      justifyContent: justify ?? undefined,
    }}>
      {title && <h4 className={classes.BlockHeaderTitle}>{title}</h4>}
      {children}
    </div>
  )
}
