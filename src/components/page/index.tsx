import { PageHeader, Spin } from "antd"
import classNames from "classnames"
import * as _ from "lodash"
import * as React from "react"
import "./index.scss"

interface ContentProps extends React.HTMLProps<HTMLSelectElement> {
  loading?: boolean
}

function Content(props: ContentProps) {
  const { className, loading, ...restProps } = props
  return <Spin spinning={_.isBoolean(loading) && loading}>
    <section className={classNames("page-content", className)} {...restProps} />
  </Spin>
}

export default class Page {
  public static Header = PageHeader
  public static Content = Content
}
