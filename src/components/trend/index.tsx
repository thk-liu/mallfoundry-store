import * as React from "react"
import "./index.scss"

interface TrendProps {
  children?: React.ReactNode
}

export default function Trend(props: TrendProps) {
  const { children } = props
  return (
    <div className="trend">
      {children}
    </div>
  )
}
