import * as React from "react"

interface DisplayProps {
  show?: boolean
  children?: React.ReactNode
}

export default function Display(props: DisplayProps) {
  const { show, children } = props
  return (
    <div style={{
      ...show ? {} : { display: "none" },
    }}>
      {children}
    </div>
  )
}
