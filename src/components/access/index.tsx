import { SubjectHolder } from "@mallfoundry/keystone/security"
import * as _ from "lodash"
import * as React from "react"
import { RouteProps } from "react-router"
import { Redirect, Route } from "react-router-dom"

interface PrivateRouteProps extends RouteProps {

}

export default function PrivateRoute(props: PrivateRouteProps) {
  const { component, children, ...rest } = props
  let node = children
  if (_.isEmpty(node)) {
    // @ts-ignore
    node = React.createElement(component, rest)
  }
  console.log(SubjectHolder.authenticated)
  return (
    <Route
      {...rest}
      render={({ location }) =>
        SubjectHolder.authenticated ? (
          node
        ) : (
          <div>
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location },
              }}
            />
          </div>
        )
      }
    />
  )
}
