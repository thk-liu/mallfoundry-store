import { addInterceptor, configure } from "@mallfoundry/client"
import { AxiosHttpClient } from "@mallfoundry/client/axios"
import { AuthorizationInterceptor } from "@mallfoundry/keystone/security"
import * as  React from "react"
import * as ReactDOM from "react-dom"
import App from "./App"
import "./index.css"
import * as serviceWorker from "./serviceWorker"

configure({
  clientClass: AxiosHttpClient,
  // urls: "http://api2.mallfoundry.com/",
  urls: "http://localhost:8077/",
})

addInterceptor(new AuthorizationInterceptor())

ReactDOM.render(<App/>, document.getElementById("root"))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
