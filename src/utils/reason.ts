import { getResponseMessage } from "@mallfoundry/client/message"
import { message } from "antd"

export function messageError(reason: any) {
  message.error(getResponseMessage(reason))
}
