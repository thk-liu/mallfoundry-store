import { User, UserService } from "@mallfoundry/keystone/identity"
import { useEffect, useState } from "react"

export function useCurrentUser() {
  const [user, setUser] = useState(new User())
  useEffect(() => {
    UserService.getCurrentUser().then(setUser)
  }, [])
  return user
}
