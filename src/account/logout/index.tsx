import { SubjectHolder } from "@mallfoundry/keystone/security"
import * as React from "react"
import { useEffect } from "react"
import { useHistory } from "react-router-dom"

export default function Logout() {
  const history = useHistory()
  useEffect(() => {
    SubjectHolder.logout().then(() => history.replace(`/`))
  }, [history])
  return <div>正在退出...</div>
}
